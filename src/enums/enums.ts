export enum ADMIN_TYPE {
  ADMIN = 1,
  ADMIN_USER = 2,
  COMPANY = 3,
  COMPANY_USER = 4
}

export enum STATUS {
  DEFAULT = 0,
  ACTIVE = 1,
  IN_ACTIVATED = 2,
  DELETED = 3,
  PENDING = 4,
  REFUND = 5,
  COMPLETED = 6
}

export enum DEVICE_TYPE {
  DEFAULT = 0,
  ANDROID = 1,
  IOS = 2,
  WEB = 3,
}

export enum PAYMENT_MODE {
  APPLE_PAY = 1,
  CASH = 2,
  CARD = 3,
  NO_PAYMENT = 4,
}

export enum MESSAGE_TYPE {
  DEFAULT = 0,
  CHAT = 1,
  ADMIN = 2,
}

export enum LOGIN_TYPE {
  DEFAULT = 0,
  NORMAL = 1,
  GOOGLE = 2,
  FACEBOOK = 3,
}

export enum CATALOGUE_TYPE {
  CATEGORY=1,
  SERVICE=2,
  PRODUCT=3
}
