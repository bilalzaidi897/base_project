import express,{NextFunction} from "express";
import http from "http";
import { json, urlencoded } from "body-parser";
import cors from "cors";
import SERVER from "./config/environment";
import logger from "./services/logger";
import connectMongoDB from "./connection/mongoose";
import connectRedisDB from "./connection/reids";
import {elasticSearch} from "./modules/v1/common/dao/elasticSearch";
import {bullInitialize} from "./services/bull";
import {socketInitialize} from "./services/socket";
import routes from "./modules/v1";
import {apiUrls} from "./urls/apiUrls";
import {sendErrorResponse} from "./lib/universal-function"
import {createAdminUser} from "./boostrap/initialize";

const app = express();
const server = http.createServer(app);
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(cors());

app.use((req:any, res:any, next:NextFunction) => {
  if (apiUrls[req.url]) {
    logger.log(req)
  }
  next();
});
app.use("/api/v1",routes)
app.use((error:any, req:any, res:any, next:NextFunction) => {
  if (apiUrls[req.url]) {
    logger.log(error,req)
  }
  logger.error(error)
  return sendErrorResponse(req, res,error?.code,(error?.message || error));
});
app.listen(SERVER.PORT, async () => {
  try {
   await connectRedisDB.connectRedis();
  //  await elasticSearch.init();
   logger.info("Server Listen.", process.env.NODE_ENV);
    await connectMongoDB();
    await bullInitialize();
    await socketInitialize(server);
    await createAdminUser();
  } catch (error) {
    logger.log("Server Listener error.", JSON.stringify(error));
    logger.warn("Server Listener error.", JSON.stringify(error));
    process.exit(0);
  }
});