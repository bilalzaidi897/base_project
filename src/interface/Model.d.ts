declare interface User {
	userId?: string;
	adminId?: string;
	parentId?:string;
	adminType?: 1 | 2 | 3| 4;
}

declare interface Device extends User {
	platform?: string;
	deviceId?: string;
	deviceType?:string;
	deviceName?:string;
	deviceToken?: string;
	refreshToken?: string;
	accessToken?: string;
	ip?: string;
	apiKey?:string;
	timezone?: number;
}
declare interface TokenData extends Device {
	socialLoginType?: string;
	isLogoutAllDevice?:boolean;
	socialId?: string;
	permission?: string[];
	adminType?: string;
	createdAt?: number;
}

declare interface DeeplinkRequest {
	android?: string;
	ios?: string;
	fallback?: string;
	token: string;
	name: string;
	type?: string;
	accountLevel: string;
}

// Model Type For DAO manager
declare type ModelNames =
"Admins" | 
"AdminSessions" |
"Catalogues" | 
"Users" |
"Sessions" | 
"Roles" | 
"Permissions" | 
"Modules" | 
"Settings" |
"Transactions" |
"UserNotifications"