const enum Models {
	ADMIN="Admins",
	ADMIN_SESSIONS="AdminSessions",
	CATALOGUES="Catalogues",
	USERS="Users",
	USER_SESSIONS="Sessions",
	ROLES="Roles",
	PERMISSIONS="Permissions",
	MODULES="Modules",
	SETTINGS="Settings",
	TRANSACTIONS="Transactions",
	USER_NOTIFICATIONS="UserNotifications"
}
