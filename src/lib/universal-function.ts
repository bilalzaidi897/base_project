import { Request, Response } from "express";
import * as jwt from 'jsonwebtoken'
import bcrypt from "bcryptjs";
import * as fs from "fs";
import path from "path";
import crypto from "crypto";
import { compile } from "handlebars";
import messages from "../messages/messages";
import * as language from "../langs";
import SERVER from "../config/environment";
import CONSTANT from "../constant/constant";
import MESSAGES from "../messages/messages";
import codes from "../codes/status_codes";
import {CustomError} from "../modules/v1/common/dao/customError"
let langs:any = language;
const sendResponse = async (
  req: any,
  res: any,
  code?: number,
  message?: string | any,
  data?: any
) => {

  const lang:string = req.headers["content-language"] || CONSTANT.LANGUAGE_TYPE.ENGLISH;
  return res.status(200).send({
    statusCode: code || codes.SUCCESS,
    message: langs[lang][message || messages.SUCCESS],
    data: data || {},
  });
};
const sendErrorResponse = async (
  req: any,
  res: any,
  code?: number,
  error?: any
) => {
  const lang:string = req.headers["content-language"] || CONSTANT.LANGUAGE_TYPE.ENGLISH;
  code = code || codes.BAD_REQUEST;
  return res.status(code).send({
    statusCode:code, 
    error: error,
    message: langs[lang][error] || error,
  });
};

const validateSchema = async (inputs: any, schema: any) => {
  try {
    const { error, value } = schema.validate(inputs);
    if (error) throw error.details ? error.details[0].message : "";
    else return false;
  } catch (error) {
    throw error;
  }
};

const renderMessage = async (templateData: any, variablesData: any) => {
  return compile(templateData)(variablesData);
};
const generateNumber = async () => {
  return Math.floor(1000 + Math.random() * 9000).toString();
};
const filterSpacesFromArray = async (arr: any[]) => {
  return arr.filter((item) => {
    return item && /\S/.test(item);
  });
};
const removeNewLineCharacters = async (value: string) => {
  if (!value) {
    return value;
  }
  return value.toString().replace(/\n|\r/g, "");
};
const dynamicPrecision = async (value: string, precision: number) => {
  precision = precision || 2;
  let float_val = parseFloat(value);
  if (isNaN(float_val)) {
    return value;
  }
  return +float_val.toFixed(precision);
};
const convertArrayToObject = async (array: []) => {
  let arrayLength = array.length;
  let returnObj = {};
  for (let count = 0; count < arrayLength; count++) {
    returnObj[array[count]] = array[count];
  }
  return returnObj;
};
const addingSecurePrefixToURL = async (domainURL: string) => {
  if (domainURL.indexOf("https://") < 0 && domainURL.indexOf("http://") < 0) {
    domainURL = "https://" + domainURL;
  }
  return domainURL;
};
const delay = (ms: number) => new Promise((res) => setTimeout(res, ms));

const jwtSign = async (payload: any) => {
  try {
    return jwt.sign(payload, SERVER.JWT.SECRET || "", {
      expiresIn:SERVER.JWT.EXPIRESIN?(parseInt(SERVER.JWT.EXPIRESIN)* 1000):0 ,
    });
  } catch (error) {
    throw new CustomError(MESSAGES.INVALID_SECRET_KEY,{code:codes.BAD_REQUEST})
  }
};

const jwtVerify = async (token: string) => {
  try {
    return jwt.verify(token, SERVER.JWT.SECRET || "");
  } catch (error) {
    throw new CustomError(MESSAGES.UNAUTHORIZED,{code:codes.UNAUTHORIZED})
  }
};
const hashPasswordUsingBcrypt = async (plainTextPassword: string) => {
  return bcrypt.hashSync(plainTextPassword, 10);
};

const comparePasswordUsingBcrypt = async (pass: string, hash: string) => {
  return bcrypt.compareSync(pass, hash);
};
const readFile = async (filePath: string) => {
  return fs.readFileSync(
    path.join(process.cwd()) + "/views/" + filePath,
    "utf8"
  );
};
const randomString = async (len?: number) => {
  len = len || 20;
  return crypto.randomBytes(20).toString("hex") + "-" + new Date().getTime();
};
const dateDifference = async (startDate: Date, endDate: Date) => {
  return endDate.getTime() - startDate.getTime();
};
const isEmpty = (value:any )=>{
  return (value === undefined || value === null  || Number.isNaN(value) || 
  (typeof value === 'object' && Object.keys(value).length === 0) || 
  (typeof value === 'string' && (value.trim()).length === 0));
}
const isJson = async (params: any) => {
  try {
    return JSON.parse(params);
  } catch (error) {
    return false;
  }
}
const findReverseHierarchy=async(data:any, key:any, level = 20) =>{
  const result = [];
  // Find the user with the provided ID
  const currentData = data.find((el:any) => el.id === key);
  // If the user is found, recursively find the reverse hierarchy
  if (currentData) {
    const { id,level, reporting_to } = currentData;
    result.push({id});
    // If the current level is greater than 0, continue the reverse hierarchy
    if (level > 0 && reporting_to !== null) {
      const subHierarchy:any = await findReverseHierarchy(data, reporting_to, level - 1);
      result.push(...subHierarchy);
    }
  }
  return result;
}
const tree = (data:any, id =  null, link = 'reporting_to') =>
data
    .filter((item:any) => item[link] === id)
    .map((item:any) => ({ ...item, children: tree(data, item.id) }));

const getWhatsAppDate =(timestamp:number) =>{
      const today = new Date();
      const date = new Date(timestamp);
    
      // Check if the date is today
      if (date.toDateString() === today.toDateString()) {
        return 'today';
      }
    
      // Check if the date is yesterday
      const yesterday = new Date(today);
      yesterday.setDate(today.getDate() - 1);
      if (date.toDateString() === yesterday.toDateString()) {
        return 'yesterday';
      }
    
      // Return the actual date
      const options:any = { year: 'numeric', month: 'long', day: 'numeric' };
      return date.toLocaleDateString(undefined, options);
}
    

export {
  sendResponse,
  sendErrorResponse,
  validateSchema,
  renderMessage,
  generateNumber,
  filterSpacesFromArray,
  removeNewLineCharacters,
  dynamicPrecision,
  convertArrayToObject,
  addingSecurePrefixToURL,
  delay,
  jwtVerify,
  hashPasswordUsingBcrypt,
  comparePasswordUsingBcrypt,
  readFile,
  jwtSign,
  randomString,
  dateDifference,
  isEmpty,
  isJson,
  findReverseHierarchy,
  tree,
  getWhatsAppDate
};
