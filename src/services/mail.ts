import nodemailer from "nodemailer";
import ses from  "nodemailer-ses-transport";
import SERVER from "../config/environment";
import CONSTANT from "../constant/constant";
import logger from "../services/logger";
const fromEmail = SERVER.FROM_EMAIL;
// using smtp
let smtpOpions:any={
  host: SERVER.SMTP.HOST,
  port: SERVER.SMTP.PORT,
  secure: true, // use SSL
  auth: {
    user: SERVER.SMTP.USER,
    pass: SERVER.SMTP.PASSWORD,
  }
}
let sesOptions:any={
  accessKeyId: SERVER.SES.ACCESS_KEY_ID,
  secretAccessKey: SERVER.SES.SECRET_ACCESS_KEY,
}
const transporter = nodemailer.createTransport(smtpOpions);

// using Amazon SES
const sesTransporter = nodemailer.createTransport(
  ses(sesOptions)
);

const sendMailViaSmtp = async (params:any) => {
  try {
    let mailOptions:any = {
      from: `${CONSTANT.PROJECT_NAME} <${fromEmail}>`,
      to: params.email,
      subject: params.subject,
      html: params.content,
    };
    if (params.bcc) mailOptions["bcc"] = params["bcc"];
    if (params.attachments) mailOptions["attachments"] = params["attachments"];
    return new Promise(function (resolve, reject) {
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
          resolve(false);
        } else {
          console.log("Message sent: " + info.response);
          resolve(true);
        }
      });
    });
  } catch (error) {
    console.log(error);
  }
};

const sendMailViaAmazonSes = async (params:any) => {
  try {
    let mailOptions:any = {
      from: `${SERVER.APP_NAME} <${fromEmail}>`,
      to: params.email,
      subject: params.subject,
      html: params.content,
    };
    if (params.bcc) mailOptions["bcc"] = params["bcc"];
    if (params.attachments) {
      mailOptions["attachments"] = [
        {
          filename: "label",
          content: params.base64,
          encoding: "base64",
        },
      ];
    }
    return new Promise((resolve, reject) => {
      sesTransporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
          resolve(false);
        } else {
          console.log("Message sent: " + info.response);
          resolve(true);
        }
      });
    });
  } catch (error) {
    console.log(error);
  }
};

const sendMail = async (params:any) => {
  //let {email,subject,content}=params
  switch (CONSTANT.SERVICE_TYPE.SMTP) {
    case CONSTANT.SERVICE_TYPE.SMTP:
      return await sendMailViaSmtp(params);
    case CONSTANT.SERVICE_TYPE.SES:
      return await sendMailViaAmazonSes(params);
  }
};
export  { sendMail };
