import { createLogger, format, transports } from "winston";
import  *  as  winston  from  'winston';
let { combine, timestamp, label, printf } = format;
import  'winston-daily-rotate-file';
import CONSTANT from "../constant/constant";

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}:${message}`;
});
const dailyTransportOpts = {
  filename: "./logs/application-%DATE%.log",
  datePattern: "YYYY-MM-DD-HH",
  zippedArchive: true,
  maxSize: "1k", //20m
  maxFiles: "14d",
};

let transportArray = [];

if (CONSTANT.LOGGER.ADD_ERROR_FILE)
  transportArray.push(
    new transports.File({
      filename: "./logs/error.log",
      level: "error",
      format: format.combine(
        // Render in one line in your log file.
        // If you use prettyPrint() here it will be really
        // difficult to exploit your logs files afterwards.
        format.json()
      ),
    })
  );

if (CONSTANT.LOGGER.ADD_INFO_FILE)
  transportArray.push(
    new transports.File({
      filename: "./logs/info.log",
      format: format.combine(
        // Render in one line in your log file.
        // If you use prettyPrint() here it will be really
        // difficult to exploit your logs files afterwards.
        format.json()
      ),
    })
  );

if (CONSTANT.LOGGER.CONSOLE_ON) transportArray.push(new transports.Console());

if (CONSTANT.LOGGER.DAILY_ROTATE_LOG_ON)
transportArray.push(new winston.transports.DailyRotateFile(dailyTransportOpts));

const logger = createLogger({
  format: combine(
    label({ label: "Admin Service" }),
    timestamp(),
    myFormat,
    //format.prettyPrint()
  ),
  transports: transportArray,
});
export default logger ;
