import SERVER from "../config/environment";
import CONSTANT from "../constant/constant";
import logger from "../services/logger";
import AWS from "aws-sdk";
// Twilio Constants
import * as TwilioSDK from "twilio";
let client=new TwilioSDK.Twilio(SERVER.TWILIO.ACCOUNT_SID, SERVER.TWILIO.AUTH_TOKEN);
const TWILIO_NUMBER =SERVER.TWILIO.TWILIO_NUMBER ;

const sendMessageViaTwilio=async(phoneNumber:string, body:any, otpType = CONSTANT.SMS_TYPE.TEXT)=>{
    if (otpType === CONSTANT.SMS_TYPE.TEXT) {
        return new Promise(function (resolve, reject) {
            return client.messages.create({
                to: `+${phoneNumber}`,
                from: TWILIO_NUMBER,
                body: body
            })
                .then(function (data:any) {
                    console.log("MessageID is " + JSON.stringify(data));
                    resolve(true);
                })
                .catch(function (error:any) {
                    console.log("Error MessageID is " + JSON.stringify(body));
                    console.error(error);
                    resolve(true);
                });
        });
    } else { // voice call
        return client.calls
            .create({
                from: TWILIO_NUMBER ||'',
                to: `+${phoneNumber}`,
                twiml: body
            })
            .then((call:any) => console.log(call.sid));
    }
};
const sendMessageViaAwsSns=async(phoneNumber:string, body:any)=>{
    try {
        // Set region
        AWS.config.update({
            region: SERVER.SNS.REGION,
            accessKeyId: SERVER.SNS.ACCESS_KEY_ID,
            secretAccessKey: SERVER.SNS.SECRET_ACCESS_KEY
        });

        // Create promise and SNS service object
        const publishTextPromise = new AWS.SNS({ apiVersion: SERVER.SNS.API_VERSION }).publish({
            Message: body,
            PhoneNumber: `+${phoneNumber}`,
        }).promise();

        return new Promise(function (resolve, reject) {
            return publishTextPromise
                .then(function (data) {
                    console.log("MessageID is " + data.MessageId);
                    resolve(true);
                })
                .catch(function (error) {
                    console.log(error);
                    resolve( true);
                });
        });
    } catch (error) {
        throw error;
    }
};
const sendSms=async (phoneNumber:string, body:any) =>{
    //countryCode + "" + mobileNo, sms
    switch (CONSTANT.SERVICE_TYPE.TWILIO) {
        case CONSTANT.SERVICE_TYPE.TWILIO:
            return sendMessageViaTwilio(phoneNumber, body);
        case CONSTANT.SERVICE_TYPE.SNS:
            return sendMessageViaAwsSns(phoneNumber, body);
    }
};

export { sendSms }


