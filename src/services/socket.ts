import socket from 'socket.io'
import SERVER from "../config/environment";
import CONSTANT from "../constant/constant";
import { hGetData, hmSetData, hDeleteData, deleteData, hlen, keys, hgetallData, expireKey } from "./redis";
import { delay } from "./../lib/universal-function";

let socketPrefix = CONSTANT.PREFIX.SOCKET;

const socketInitialize = async(httpsServer:any) => {
    let socketIO: socket.Server = httpsServer;

    process.on("app", function(data) {
        socketIO.to(data.id).emit("app", data);
    });
    process.on("dashboard", function(data) {
        socketIO.to(data.id).emit("dashboard", data);
    });

    socketIO.on("connection", async(socket:any) => {
        console.log("connected")
        if (socket.handshake.query && socket.handshake.query.id) {
            console.log("connected", socket.handshake.query.id, socket.id)
            let obj = {
                [socket.id]: socket.handshake.query.id,
            };
            socket.join(socket.handshake.query.id);
            await hmSetData(socketPrefix, obj);
            await expireKey(socketPrefix, CONSTANT.EXPIRE_TIME.SOCKET_EXPIRATION_TIME)

        }
       
        socket.on("disconnect", async function(data:any) {
            let id = await hGetData(socketPrefix, socket.id);
            console.log("disconnect", id, socket.id)
            socket.leave(id);
            await hDeleteData(socketPrefix, socket.id);
            socket.conn.close();
        });
        
        socket.on("ping", async function(data:any) {
            socket.emit("pong", data);
        });
    });

    // async function getActiveRooms() {
    //     // Convert map into 2D list:
    //     // ==> [['4ziBKG9XFS06NdtVAAAH', Set(1)], ['room1', Set(2)], ...]
    //     let arr = Array.from(socketIO.sockets.adapter.rooms);
    //     // Filter rooms whose name exist in set:
    //     // ==> [['room1', Set(2)], ['room2', Set(2)]]
    //     const filtered = arr.filter(room => !room[1].has(room[0]))
    //         // Return only the room name: 
    //         // ==> ['room1', 'room2']
    //     const res = filtered.map(i => i[0]);
    //     return res;
    // }

    // async function activeSocket() {
    //     // Convert map into 2D list:
    //     // ==> [['4ziBKG9XFS06NdtVAAAH', Set(1)], ['room1', Set(2)], ...]
    //     let arr = Array.from(socketIO.sockets.adapter.rooms);
    //     // Filter rooms whose name exist in set:
    //     // ==> [['room1', Set(2)], ['room2', Set(2)]]
    //     const filtered = arr.filter(room => room[1].has(room[0]))
    //         // Return only the room name: 
    //         // ==> ['room1', 'room2']
    //     const res = filtered.map(i => i[0]);
    //     return res;
    // }

};
const deleteAllSocket = async() => {
    await deleteData(socketPrefix);

}
export { socketInitialize, deleteAllSocket }