import axios from "axios";
import qs from "qs";

import CONSTANT from "../constant/constant";
import logger from "../services/logger";

const formUrlEncoded = (x: any) =>
  Object.keys(x).reduce((p, c) => p + `&${c}=${encodeURIComponent(x[c])}`, "");

const sendRequest = async (
  method?: string,
  type?: string,
  url?: string,
  params?: any,
  data = {},
  headers = {}
) => {
  try {
    method = method ? method.toLowerCase() : "get";
    let paramsSerializer;
    let requestObj: any = {
      url: url,
      method,
      headers,
      params,
      data,
      paramsSerializer,
    };
    switch (type) {
      case CONSTANT.REQUEST_METHOD_TYPE.GET:
        method = CONSTANT.REQUEST_METHOD_TYPE.GET;
        delete requestObj.data;
        let paramsSerializer = function (params: any) {
          return qs.stringify(params, {
            arrayFormat: "brackets",
          });
        };
        requestObj.paramsSerializer = paramsSerializer;
        break;
      case CONSTANT.REQUEST_METHOD_TYPE.POST:
        method = CONSTANT.REQUEST_METHOD_TYPE.POST;
        delete requestObj.params;
        delete requestObj.paramsSerializer;
        requestObj.data = data;
        break;
      case CONSTANT.REQUEST_METHOD_TYPE.FORM_POST:
        method = CONSTANT.REQUEST_METHOD_TYPE.POST;
        delete requestObj.params;
        delete requestObj.paramsSerializer;
        requestObj.data = formUrlEncoded(data);
        break;
      case CONSTANT.REQUEST_METHOD_TYPE.PUT:
        method = CONSTANT.REQUEST_METHOD_TYPE.PUT;
        delete requestObj.params;
        delete requestObj.paramsSerializer;
        requestObj.data = data;
        break;
      case CONSTANT.REQUEST_METHOD_TYPE.DELETE:
        method = CONSTANT.REQUEST_METHOD_TYPE.DELETE;
        delete requestObj.params;
        delete requestObj.paramsSerializer;
        requestObj.data = data;
        break;
    }
    requestObj.headers = Object.assign({}, headers);
    const response = await axios(requestObj);
    return {
      status: response.status,
      message: response.statusText,
      data: response.data,
    };
  } catch (error: any) {
     logger.warn('Axios service throw error.', JSON.stringify(error));
    return {
      status: 208,
      message: error?.message,
      data: {},
    };
  }
};
const sendGET = async (url: string, params: any, data = {}, headers = {}) => {
  return await sendRequest(
    CONSTANT.REQUEST_METHOD_TYPE.GET,
    CONSTANT.REQUEST_METHOD_TYPE.GET,
    url,
    params,
    data,
    headers
  );
};
const sendPOST = async (url: string, params: any, data = {}, headers = {}) => {
  return await sendRequest(
    CONSTANT.REQUEST_METHOD_TYPE.POST,
    CONSTANT.REQUEST_METHOD_TYPE.POST,
    url,
    params,
    data,
    headers
  );
};
const sendFormPOST = async (
  url: string,
  params: any,
  data = {},
  headers = {}
) => {
  return await sendRequest(
    CONSTANT.REQUEST_METHOD_TYPE.POST,
    CONSTANT.REQUEST_METHOD_TYPE.FORM_POST,
    url,
    params,
    data,
    headers
  );
};
const sendPUT = async (url: string, params: any, data = {}, headers = {}) => {
  return await sendRequest(
    CONSTANT.REQUEST_METHOD_TYPE.PUT,
    CONSTANT.REQUEST_METHOD_TYPE.PUT,
    url,
    params,
    data,
    headers
  );
};
const sendDELETE = async (
  url: string,
  params: any,
  data = {},
  headers = {}
) => {
  return await sendRequest(
    CONSTANT.REQUEST_METHOD_TYPE.DELETE,
    CONSTANT.REQUEST_METHOD_TYPE.DELETE,
    url,
    params,
    data,
    headers
  );
};

export default {
  sendGET,
  sendPOST,
  sendFormPOST,
  sendPUT,
  sendDELETE,
};
