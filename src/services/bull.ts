import Queue from "bull";
import SERVER from "../config/environment";
import logger from "../services/logger";

const redisConnection:Queue.QueueOptions={
    redis: {
      port: parseInt(SERVER.REDIS.PORT || ''),
      host: SERVER.REDIS.HOST,
      password: SERVER.REDIS.PASSWORD
    }
}
const eventQueue = new Queue("events",redisConnection );

const bullInitialize = async () => {
  try {
    eventQueue.process(function (job:any, done) {
      logger.info("Job process in queue.", JSON.stringify(job));
      let { data, id, jobId, name, opts, event } = job;
      switch (event) {
        default:
          break;
      }
      done();
    });
  } catch (error) {
    logger.warn("Bull service throw error.", JSON.stringify(error));
    return true;
  }
};

const addJob = async (jobData:any, options:any) => {
  try {
    let { jobId } = jobData;
    logger.info("Add job in queue.", JSON.stringify(jobData));
    if (options.hasOwnProperty("repeat") && jobId) removeJob(jobId);

    return eventQueue.add(jobData, options);
  } catch (error) {
    logger.warn("addJob throw error.", JSON.stringify(error));
    return true;
  }
};
const removeJob = async (jobId:string) => {
  try {
    logger.info("Remove job in queue.", JSON.stringify(jobId));
    eventQueue.removeJobs(jobId);
  } catch (error) {
    logger.warn("removeJob throw error.", JSON.stringify(error));
    return true;
  }
};
const removeJobByKey = async (jobId:string) => {
  try {
    logger.info("Remove job in queue by key.", JSON.stringify(jobId));
    eventQueue.removeRepeatableByKey(jobId);
  } catch (error) {
    logger.warn("removeJobByKey throw error.", JSON.stringify(error));
    return true;
  }
};
export { bullInitialize, addJob, removeJob, removeJobByKey };
