import redisConnection from "../connection/reids";
let redisClient=redisConnection.redisClient;
const setData = (key:string, value:string) => {
    return redisClient.set(key, value);
}
const getData = (key:string) => {
    return redisClient.get(key);
}
const deleteData = (key:string) => {
    return redisClient.del(key);
}
const hlen = (key:string) => {
    return redisClient.hlen(key);
}
const hSetData = (hkey:any, key:string, value:any) => {
    return redisClient.hset(hkey, key, value);
}
const hGetData = (hkey:any, key:string) => {
    return redisClient.hget(hkey, key);
}
const hDeleteData = (hkey:any, field:string) => {
    return redisClient.hdel(hkey, field);
}
const hExistsData = (hkey:any, key:string) => {
    return redisClient.hexists(hkey, key);
}
const hmSetData = (hkey:any, obj:any) => {
    return redisClient.hmset(hkey, obj);
}
const hmGetData = (hmkey:any, arr:[]) => {
    return redisClient.hmget(hmkey, arr);
}
const hgetallData = (hmkey:any) => {
    return redisClient.hgetall(hmkey);
}
const hmDeleteData = (hmkey:any) => {
    return redisClient.hmdel(hmkey);
}
const hmExistsData = (hkey:any, key:string) => {
    return redisClient.hmexists(hkey, key);
}
const keys = (keyPattern:any) => {
    return redisClient.keys(keyPattern);
}
const expireKey = (key:string, expiryTime:number) => {
    return redisClient.expire(key, expiryTime);
}
export {
    getData,
    setData,
    deleteData,
    hSetData,
    hGetData,
    hDeleteData,
    hExistsData,
    hmSetData,
    hmGetData,
    hmDeleteData,
    hmExistsData,
    hlen,
    keys,
    hgetallData,
    expireKey
}