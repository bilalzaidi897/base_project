
import fs from "fs";
import AWSSDK from "aws-sdk";
import gm from "gm";
gm.subClass({ imageMagick: true });
import SERVER from "../config/environment";
const accessKeyId = SERVER.S3.ACCESS_KEY_ID;
const secretAccessKey = SERVER.S3.SECRET_ACCESS_KEY;
const bucketName = SERVER.S3.BUCKET
const Url = SERVER.S3.URL

const uploadOriginalImageToS3Bucket = async (file:any, folder:string) => {
    return new Promise((resolve, reject) => {
        try {
            var image_name = file.name.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, "");
            var image_size = file.size;
            var timestamp = new Date().getTime().toString();
            var str = "";
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var size = chars.length;
            for (var i = 0; i < 4; i++) {
                var randomnumber = Math.floor(Math.random() * size);
                str = chars[randomnumber] + str;
            }
            image_name = image_name.replace(/\s/g, "");
            image_name = str + timestamp + "-" + image_name;
            file.name = image_name;
            if (file.flag) file.name = file.name + "." + file.ext;
            var filename = file.name; // actual filename of file
            var path = file.path; //will be put into a temp directory
            var mimeType = file.type;

            fs.readFile(path, function (error:any, file_buffer:any) {
                if (error) {
                    return resolve(0);
                }
                filename = file.name;
                AWSSDK.config.update({
                    accessKeyId: accessKeyId,
                    secretAccessKey: secretAccessKey,
                });
                var s3bucket = new AWSSDK.S3();
                var params:any = {
                    Bucket: bucketName,
                    Key: folder + "/" + filename,
                    Body: file_buffer,
                    ACL: "public-read",
                    ContentType: mimeType,
                };
                s3bucket.putObject(params, function (err:any, data:any) {
                    if (err) {
                        return resolve(0);
                    }
                    return resolve(Url + folder + "/" + filename);
                });
            });
        } catch (e) {
            return resolve(0);
        }
    });
};
/*
 * -----------------------------------------------------------------------------
 * Uploading Thumb image to s3 bucket
 * INPUT : file parameter
 * OUTPUT : image path
 * -----------------------------------------------------------------------------
 */
const uploadThumbImageToS3Bucket = async (file:any, folder:string) => {
    return new Promise((resolve, reject) => {
        var image_name = file.name.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, "");
        var image_size = file.size;
        var timestamp = new Date().getTime().toString();
        var str = "";
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var size = chars.length;
        for (var i = 0; i < 4; i++) {
            var randomnumber = Math.floor(Math.random() * size);
            str = chars[randomnumber] + str;
        }
        image_name = image_name.replace(/\s/g, "");
        image_name = str + timestamp + "-" + image_name;
        file.name = image_name;
        var path = file.path; //will be put into a temp directory
        var tmp_path = file.path + "-tmpPath"; //will be put into a temp directory
        gm(path)
            .resize(150, 150, "!")
            .autoOrient()
            .write(tmp_path, function (err:any) {
                if (err) {
                    console.log("err", err);
                    return resolve(0);
                }
                var filename = file.name; // actual filename of file
                var mimeType = file.type;
                fs.readFile(tmp_path, function (error:any, file_buffer:any) {
                    if (error) {
                        console.log(error);
                        return resolve(0);
                    }
                    filename = "thumb-" + file.name;
                    AWSSDK.config.update({ accessKeyId: accessKeyId, secretAccessKey: secretAccessKey });
                    var s3bucket = new AWSSDK.S3();
                    var params:any = {
                        Bucket: bucketName,
                        Key: folder + "/" + filename,
                        Body: file_buffer,
                        ACL: "public-read",
                        ContentType: mimeType,
                    };

                    s3bucket.putObject(params, function (err:any, data:any) {
                        if (err) {
                            console.log(err);
                            return resolve(0);
                        }
                        return resolve(Url + folder + "/" + filename);
                    });
                });
            });
    });
};
const uploadImageToS3Bucket = async (file:any, folder:string) => {
    let dataToSend:any = {
        original: "",
        thumbNail: "",
    };
    dataToSend.original = await uploadOriginalImageToS3Bucket(file, folder);
    dataToSend.thumbNail = await uploadThumbImageToS3Bucket(file, folder);
    return dataToSend;
};
export  {
    uploadImageToS3Bucket,
    uploadOriginalImageToS3Bucket,
    uploadThumbImageToS3Bucket
};
