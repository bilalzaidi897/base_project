import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import {
  checkModuleExists,
  createModule,
  updateModule,
  getModuleDetails,
  getAllModules,
} from "../services/modules";
import { IModules } from "../interfaces/modules";

/**
 *
 * @param {*} params
 * @description for creating module
 * @returns
 */
const addModule = asyncHandler(async (req: Request, res: Response) => {
  let param = req.body;
  await checkModuleExists(param);
  await createModule(param);
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.MODULE_CREATE_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for edit module
 * @returns
 */
const editModule = asyncHandler(async (req: Request, res: Response) => {
  let param = req.body;
  await checkModuleExists(param);
  await updateModule(param);

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.MODULE_UPDATED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for module details
 * @returns
 */
const moduleDetails = asyncHandler(async (req: Request, res: Response) => {
  let { moduleId } = req.params;
  let module = await getModuleDetails({ moduleId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, module);
});
/**
 *
 * @param {*} params
 * @description for modules
 * @returns
 */
const modules = asyncHandler(async (req: Request, res: Response) => {
  let params = req.query as unknown as IModules;
  let modules = await getAllModules(params);
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, modules);
});

export { addModule, editModule, moduleDetails, modules };
