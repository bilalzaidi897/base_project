import MESSAGES from "../../../../messages/messages";
import { modulesDao } from "../dao/modules";
import {
  ICheckModuleExists,
  ICreateModule,
  IUpdateModule,
  IModuleDetails,
  IModules,
} from "../interfaces/modules";
/**
 *
 * @param {*} params
 * @description for cheking module exists or not
 * @returns
 */
async function checkModuleExists(params: ICheckModuleExists) {
  const moduleExists = await modulesDao.checkModuleExists(params);
  if (moduleExists) throw new Error(MESSAGES.MODULE_ALREADY_EXISTS);
  return true;
}

/**
 *
 * @param {*} params
 * @description for create module
 * @returns
 */
async function createModule(params: ICreateModule) {
  return await modulesDao.createModule(params);
}
/**
 *
 * @param {*} params
 * @description for update module
 * @returns
 */
async function updateModule(params: IUpdateModule) {
  return await modulesDao.updateModule(params);
}
/**
 *
 * @param {*} params
 * @description for role
 * @returns
 */
async function getModuleDetails(params: IModuleDetails) {
  return await modulesDao.getModuleDetails(params);
}
/**
 *
 * @param {*} params
 * @description for modules
 * @returns
 */
async function getAllModules(params: IModules) {
  return await modulesDao.getAllModules(params);
}
export {
  checkModuleExists,
  createModule,
  updateModule,
  getModuleDetails,
  getAllModules,
};
