import { Request, Response, NextFunction } from "express";
import joi from "joi";
import { validateSchema } from "../../../../lib/universal-function";

/**
 *
 * @param {*} params
 * @description for create module
 * @returns
 */
const validateAddModule = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      name: joi.string().required(),
      add: joi.boolean().optional(),
      edit: joi.boolean().optional(),
      view: joi.boolean().optional(),
      delete: joi.boolean().optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};

/**
 *
 * @param {*} params
 * @description for update module
 * @returns
 */
const validateUpdateModule = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      moduleId: joi.string().required(),
      name: joi.string().optional(),
      add: joi.boolean().optional(),
      edit: joi.boolean().optional(),
      view: joi.boolean().optional(),
      delete: joi.boolean().optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get module
 * @returns
 */
const validateModule = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      moduleId: joi.string().required(),
    });
    await validateSchema(req.params, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get modules
 * @returns
 */
const validateModules = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};

export {
  validateAddModule,
  validateUpdateModule,
  validateModule,
  validateModules,
};
