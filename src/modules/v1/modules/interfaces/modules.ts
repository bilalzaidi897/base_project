export interface ICheckModuleExists {
    name?:string;
    moduleId?:string;
}
export interface ICreateModule{
    name:string;
    add?:boolean;
    edit?:boolean;
    view?:boolean;
    delete?:boolean;
}
export interface IUpdateModule{
    moduleId:string;
    name:string;
    add?:boolean;
    edit?:boolean;
    view?:boolean;
    delete?:boolean;
}
export interface IModuleDetails {
    moduleId:string
}
export interface IModules{
    limit:number;
    pageNo:number;
}
export interface IReadPermissions<T> {
    checkModuleExists(params:ICheckModuleExists): Promise<any>;
    getModuleDetails(params:IModuleDetails): Promise<any>;
    getAllModules(params:IModules): Promise<any>;
}
export interface IWritePermissions<T> {
    createModule(params:ICreateModule): Promise<any>;
    updateModule(params:IUpdateModule): Promise<any>;
}

