import { CATALOGUE_TYPE,STATUS } from "enums/enums";
export interface ICatalogueNameExists{
    catalogueId?:Object;
    name:string;
}
export interface ICatalogue{
    catalogueId:Object;
}
export interface ICatalogues{
    catalogueId?:Object;
    pageNo?:number;
    limit?:number;
    search?:string;
}
export interface ICreateCatalogue{
  parentId?: Object;
  categoryId?: Object;
  serviceId?: Object;
  combo?: Object[];
  name: string;
  description?: string;
  tags?:string[];
  searchTags?:string[];
  sku?:string;
  price?:number;
  profilePic?:{
    original:string;
    thumbNail:string;
  };
  translations?:Object;
  catalogueType:
    CATALOGUE_TYPE.CATEGORY |
    CATALOGUE_TYPE.SERVICE |
    CATALOGUE_TYPE.PRODUCT;
  status?:
   STATUS.DEFAULT
  | STATUS.DELETED;
}
export interface IUpdateCatalogue{
    catalogueIds?:Object[];
    catalogueId?:Object;
    parentId?: Object;
    categoryId?: Object;
    serviceId?: Object;
    combo?: Object[];
    name: string;
    description?: string;
    tags?:string[];
    searchTags?:string[];
    sku?:string;
    price?:number;
    profilePic?:{
      original:string;
      thumbNail:string;
    };
    translations?:Object;
    catalogueType:
      CATALOGUE_TYPE.CATEGORY |
      CATALOGUE_TYPE.SERVICE |
      CATALOGUE_TYPE.PRODUCT;
    status?:
     STATUS.DEFAULT
    | STATUS.DELETED;
}


export interface IReadCatalogue<T> {
    checkCatalogueExists(params:any): Promise<any>;
    catalogues(params:any): Promise<any>;
    catalogueDetails(params:any): Promise<any>;
}
export interface IWriteCatalogue<T> {
    createCatalogue(params:any): Promise<any>;
    updateCatalogue(params:any): Promise<any>;
}

