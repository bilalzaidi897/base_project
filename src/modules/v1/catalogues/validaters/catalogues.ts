import { Request, Response, NextFunction } from "express";
import joi, { object } from "joi";
import CONSTANT from "../../../../constant/constant";
import { CATALOGUE_TYPE } from "../../../../enums/enums";
import { validateSchema } from "../../../../lib/universal-function";

const translationSchema = {
  name: joi.string().required(),
  description: joi.string().required(),
};
const translations = () => {
  let translationObj={}
   Object.values(CONSTANT.LANGUAGE_TYPE).map(( language) => {
    translationObj= {...translationObj,[language]:translationSchema};
    return true;
  });
  return translationObj
};

/**
 *
 * @param {*} params
 * @description for create Catalogue
 * @returns
 */
const validateAddCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      name: joi.string().required(),
      description: joi.string().allow("").optional(),
      parentId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      categoryId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      serviceId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      combo: joi
        .array()
        .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
        .optional(),
      tags: joi.array().items(joi.string().required()).optional(),
      searchTags: joi.array().items(joi.string().required()).optional(),
      sku: joi.string().allow("").optional(),
      price: joi.number().min(0).optional(),
      profilePic:joi.object().keys({
        original: joi.string().required(),
        thumbNail: joi.string().required(),
      }).optional(),
      translations: joi.any(),
        // .object()
        // .keys({
        //   ...translations(),
        // })
        // .optional(),
      catalogueType: joi
        .number()
      //  .valid(Object.values(CATALOGUE_TYPE))
        .optional(),
    });
    console.log("schema",schema)
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};

/**
 *
 * @param {*} params
 * @description for update Catalogue
 * @returns
 */
const validateUpdateCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      name: joi.string().optional(),
      description: joi.string().allow("").optional(),
      parentId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      categoryId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      serviceId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      combo: joi
        .array()
        .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
        .optional(),
      tags: joi.array().items(joi.string().required()).optional(),
      searchTags: joi.array().items(joi.string().required()).optional(),
      sku: joi.string().allow("").optional(),
      price: joi.number().min(0).optional(),
      profilePic:joi.object().keys({
        original: joi.string().required(),
        thumbNail: joi.string().required(),
      }).optional(),
      translations: joi
        .object()
        .keys({
          ...translations,
        })
        .optional(),
      catalogueType: joi
        .number()
      //  .valid(Object.values(CATALOGUE_TYPE))
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get Catalogue
 * @returns
 */
const validateCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
    });
    await validateSchema(req.params, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get Catalogues
 * @returns
 */
const validateCatalogues = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for delete Catalogue
 * @returns
 */
const validateDeleteCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      catalogueIds: joi
        .array()
        .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
        .min(1)
        .max(100)
        .required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for change status Catalogue
 * @returns
 */
const validateChangeStatusOfCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      catalogueIds: joi
        .array()
        .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
        .min(1)
        .max(100)
        .required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
export {
  validateAddCatalogue,
  validateUpdateCatalogue,
  validateCatalogue,
  validateCatalogues,
  validateDeleteCatalogue,
  validateChangeStatusOfCatalogue,
};
