import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import {
  checkCatalogueExists,
  createCatalogue,
  updateCatalogue,
  catalogueDetails,
  catalogues,
} from "../services/catalogues";
import { ICatalogues } from "../interfaces/catalogues";
/**
 *
 * @param {*} params
 * @description for creating catalogue
 * @returns
 */
const addCatalogue = asyncHandler(async (req: Request, res: Response) => {
  await checkCatalogueExists({ ...req.body });
  await createCatalogue({ ...req.body });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.CATALOGUE_CREATE_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for edit catalogue
 * @returns
 */
const editCatalogue = asyncHandler(async (req: Request, res: Response) => {
  await checkCatalogueExists({ ...req.body });
  await updateCatalogue({ ...req.body });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.CATALOGUE_UPDATED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for catalogue details
 * @returns
 */
const getCatalogueDetails = asyncHandler(
  async (req: Request, res: Response) => {
    let { catalogueId } = req.params;
    let catalogue = await catalogueDetails({ catalogueId });
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, catalogue);
  }
);
/**
 *
 * @param {*} params
 * @description for catalogues
 * @returns
 */
const getCatalogues = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query as unknown as ICatalogues;
  let cataloguesData = await catalogues({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    catalogues: cataloguesData?.catalogues || [],
    count: cataloguesData?.count || 0,
  });
});
/**
 *
 * @param {*} params
 * @description for delete catalogues
 * @returns
 */
const deleteCatalogues = asyncHandler(async (req: Request, res: Response) => {
  let roles = await updateCatalogue({ ...req.body });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, roles);
});
/**
 *
 * @param {*} params
 * @description for change status catalogues
 * @returns
 */
const changeStatusCatalogues = asyncHandler(
  async (req: Request, res: Response) => {
    let roles = await updateCatalogue({ ...req.body });
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, roles);
  }
);
export {
  addCatalogue,
  editCatalogue,
  getCatalogueDetails,
  getCatalogues,
  deleteCatalogues,
  changeStatusCatalogues,
};
