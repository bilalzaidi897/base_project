import { Router } from "express";
const router = Router();
import {
  addCatalogue,
  editCatalogue,
  getCatalogueDetails,
  getCatalogues,
  deleteCatalogues,
  changeStatusCatalogues,
} from "../controllers/catalogues";
import {
  validateAddCatalogue,
  validateUpdateCatalogue,
  validateCatalogue,
  validateCatalogues,
  validateDeleteCatalogue,
  validateChangeStatusOfCatalogue,
} from "../validaters/catalogues";
import { adminAuthorization } from "../../common/services/authorization";
/*
CATALOGUES API'S
*/
router.post("/", validateAddCatalogue, adminAuthorization, addCatalogue);
router.put("/", validateUpdateCatalogue, adminAuthorization, editCatalogue);
router.delete(
  "/",
  validateDeleteCatalogue,
  adminAuthorization,
  deleteCatalogues
);
router.patch(
  "/",
  validateChangeStatusOfCatalogue,
  adminAuthorization,
  changeStatusCatalogues
);
router.get(
  "/:catalogueId",
  validateCatalogue,
  adminAuthorization,
  getCatalogueDetails
);
router.get("/", validateCatalogues, adminAuthorization, getCatalogues);

export default router;
