import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  ICreateCatalogue,
  IUpdateCatalogue,
  ICatalogueNameExists,
  ICatalogue,
  ICatalogues,
  IWriteCatalogue,
  IReadCatalogue,
} from "../interfaces/catalogues";
export class CatalogueDao<T>
  extends BaseDao
  implements IReadCatalogue<T>, IWriteCatalogue<T>
{
  async checkCatalogueExists(params: ICatalogueNameExists): Promise<any> {
    let { catalogueId,name } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (name) query.name = name;
    if (catalogueId) query._id = { $ne: Object(catalogueId) };
    return await this.findOne(Models.CATALOGUES, query, {}, {}, {});
  }
  async catalogueDetails(params: ICatalogue): Promise<any> {
    let { catalogueId } = params;
    let query = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.CATALOGUES, query, {}, {}, [
      {
        path: "parentId",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
      {
        path: "categoryId",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
      {
        path: "serviceId",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
      {
        path: "combo",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
    ]);
  }
  async catalogues(params: ICatalogues): Promise<any> {
    let { search, limit = 0, pageNo } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (search) query["$or"] = [{ name: search }, { description: search }];

    let cataloguesData = await this.find(
      Models.CATALOGUES,
      query,
      {},
      {},
      {},
      { pageNo, limit },
      {}
    );
    let count = await this.count(Models.CATALOGUES, query);
    return { catalogues: cataloguesData, count };
  }
  async createCatalogue(params: ICreateCatalogue): Promise<any> {
    return await this.save(Models.CATALOGUES, params);
  }
  async updateCatalogue(params: IUpdateCatalogue): Promise<any> {
    let { catalogueId, catalogueIds } = params;
    let query:any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
    };
    if (catalogueIds) query._id = { $in: catalogueIds };
    let update = {
      $set: params,
    };
    return await this.update(Models.CATALOGUES, query, update, {multi:true});
  }
}
export const catalogueDao = new CatalogueDao();
