import MESSAGES from "../../../../messages/messages";
import { catalogueDao } from "../dao/catalogues";
import {
  ICatalogueNameExists,
  ICatalogue,
  ICatalogues,
  ICreateCatalogue,
  IUpdateCatalogue,
} from "../interfaces/catalogues";
/**
 *
 * @param {*} params
 * @description for cheking catalogue exists or not
 * @returns
 */
const checkCatalogueExists = async (params: ICatalogueNameExists) => {
  let roleExists = await catalogueDao.checkCatalogueExists(params);
  if (roleExists) throw new Error(MESSAGES.CATALOGUE_ALREADY_EXISTS);
  return true;
};

/**
 *
 * @param {*} params
 * @description for create catalogue
 * @returns
 */
const createCatalogue = async (params: ICreateCatalogue) => {
  return await catalogueDao.createCatalogue(params);
};
/**
 *
 * @param {*} params
 * @description for update catalogue
 * @returns
 */
const updateCatalogue = async (params: IUpdateCatalogue) => {
  return await catalogueDao.updateCatalogue(params);
};
/**
 *
 * @param {*} params
 * @description for catalogue
 * @returns
 */
const catalogueDetails = async (params: ICatalogue) => {
  return await catalogueDao.catalogueDetails(params);
};
/**
 *
 * @param {*} params
 * @description for catalogues
 * @returns
 */
const catalogues = async (params: ICatalogues) => {
  return await catalogueDao.catalogues(params);
};
export {
  checkCatalogueExists,
  createCatalogue,
  updateCatalogue,
  catalogueDetails,
  catalogues,
};
