import MESSAGES from "../../../../messages/messages";
import { rolesDao } from "../dao/roles";
import { 
  ICheckRoleExists,
  IRoleDetails,
  ICreateRole,
  IUpdateRole,
  IGetAllRoles
} from "../interfaces/roles";
/**
 *
 * @param {*} params
 * @description for cheking role exists or not
 * @returns
 */
const checkRoleExists = async (params: ICheckRoleExists) => {
  let roleExists = await rolesDao.checkRoleExists(params);
  if (roleExists) throw new Error(MESSAGES.ROLE_ALREADY_EXISTS);
  return true;
};

/**
 *
 * @param {*} params
 * @description for create role
 * @returns
 */
const createRole = async (params: ICreateRole) => {
  return await rolesDao.createRole(params);
};
/**
 *
 * @param {*} params
 * @description for update role
 * @returns
 */
const updateRole = async (params: IUpdateRole) => {
  return await rolesDao.updateRole(params);
};
/**
 *
 * @param {*} params
 * @description for role
 * @returns
 */
const getRoleDetails = async (params: IRoleDetails) => {
  return await rolesDao.roleDetails(params);
};
/**
 *
 * @param {*} params
 * @description for roles
 * @returns
 */
const getAllRoles=async (params:IGetAllRoles)=>{
 return await rolesDao.getAllRoles(params);
}
export {
  checkRoleExists,
  createRole,
  updateRole,
  getRoleDetails,
  getAllRoles
};
