export interface ICheckRoleExists {
    role?:string;
    roleId?:string
}
export interface IRoleDetails {
    roleId:string
}
interface IModules{
    moduleId:string;
    add?:Boolean;
    edit?:Boolean;
    view?:Boolean;
    delete?:Boolean;
}
export interface ICreateRole{
    role:string;
    description?:string;
    modules?:IModules[]
}
export interface IUpdateRole{
    roleId:string;
    role?:string;
    description?:string;
    modules?:IModules[]
}
export interface IGetAllRoles{
    search?:string;
    limit:number;
    pageNo:number;
}
export interface IReadRole<T> {
    checkRoleExists(params:any): Promise<any>;
    roleDetails(params:any): Promise<any>;
    getAllRoles(params:any): Promise<any>;
}
export interface IWriteRole<T> {
    createRole(params:any): Promise<any>;
    updateRole(params:any): Promise<any>;
}

