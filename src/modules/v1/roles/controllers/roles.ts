import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import {
  checkRoleExists,
  createRole,
  updateRole,
  getRoleDetails,
  getAllRoles,
} from "../services/roles";
import { IGetAllRoles } from "../interfaces/roles";
/**
 *
 * @param {*} params
 * @description for creating role
 * @returns
 */
const addRole = asyncHandler(async (req: Request, res: Response) => {
  let { role, description, modules } = req.body;
  await checkRoleExists({ ...req.body });
  let uniques = Object.values(
    modules.reduce((a: any, c: any) => {
      a[c.moduleId] = c;
      return a;
    }, {})
  );
  modules = uniques;
  await createRole({ role, description, modules });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.ROLE_CREATE_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for edit role
 * @returns
 */
const editRole = asyncHandler(async (req: Request, res: Response) => {
  let { roleId, role, description, modules } = req.body;
  await checkRoleExists({ ...req.body });
  if (modules) {
    let uniques = Object.values(
      modules.reduce((a: any, c: any) => {
        a[c.moduleId] = c;
        return a;
      }, {})
    );
    modules = uniques;
  }
  await updateRole({ roleId, role, description, modules });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.ROLE_UPDATED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for role details
 * @returns
 */
const roleDetails = asyncHandler(async (req: Request, res: Response) => {
  let { roleId } = req.params;
  let role = await getRoleDetails({ roleId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, role);
});
/**
 *
 * @param {*} params
 * @description for roles
 * @returns
 */
const roles = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query as unknown as IGetAllRoles;
  let roles = await getAllRoles({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, roles);
});

export { addRole, editRole, roleDetails, roles };
