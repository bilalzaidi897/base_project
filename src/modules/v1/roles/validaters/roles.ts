import {
    Request,
    Response,
    NextFunction
  } from "express";
  import joi from "joi";
  import { validateSchema } from "../../../../lib/universal-function";
  /**
   *
   * @param {*} params
   * @description for create role
   * @returns
   */
  const validateAddRole = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      let schema = joi.object().keys({
        role: joi.string().required(),
        description: joi.string().allow("").optional(),
        modules: joi
          .array()
          .items(
            joi.object().keys({
              moduleId: joi.string().required(),
              add: joi.boolean().optional(),
              edit: joi.boolean().optional(),
              view: joi.boolean().optional(),
              delete: joi.boolean().optional(),
            })
          )
          .optional(),
      });
      await validateSchema(req.body, schema);
      next();
    } catch (error) {
      next(error);
    }
  };
  
  /**
   *
   * @param {*} params
   * @description for update role
   * @returns
   */
  const validateUpdateRole = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      let schema = joi.object().keys({
        roleId: joi.string().required(),
        role: joi.string().optional(),
        description: joi.string().allow("").optional(),
        modules: joi
          .array()
          .items(
            joi.object().keys({
              moduleId: joi.string().required(),
              add: joi.boolean().optional(),
              edit: joi.boolean().optional(),
              view: joi.boolean().optional(),
              delete: joi.boolean().optional(),
            })
          )
          .optional(),
      });
      await validateSchema(req.body, schema);
      next();
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param {*} params
   * @description for get role
   * @returns
   */
  const validateRole = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      let schema = joi.object().keys({
        roleId: joi.string().required(),
      });
      await validateSchema(req.params, schema);
      next();
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param {*} params
   * @description for get roles
   * @returns
   */
  const validateRoles = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      let schema = joi.object().keys({
        search: joi.string().optional(),
        limit: joi.number().min(1).max(100).required(),
        pageNo: joi.number().min(1).required(),
      });
      await validateSchema(req.query, schema);
      next();
    } catch (error) {
      next(error);
    }
  };
  
  export { validateAddRole, validateUpdateRole, validateRole, validateRoles };
  