import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import { 
  ICheckRoleExists,
  IRoleDetails,
  ICreateRole,
  IUpdateRole,
  IGetAllRoles,
  IWriteRole, 
  IReadRole } from "../interfaces/roles";
export class RolesDao<T>
  extends BaseDao
  implements IReadRole<T>, IWriteRole<T>
{
  async checkRoleExists(params: ICheckRoleExists): Promise<any> {
    let { role, roleId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (role) query.role = role;
    if (roleId) query._id = { $ne: roleId };
    return await this.findOne(Models.ROLES,query, {}, {}, {});
  }
  async createRole(params: ICreateRole): Promise<any> {
    return await this.save(Models.ROLES, params);
  }
  async updateRole(params: IUpdateRole): Promise<any> {
    let { roleId } = params;
    let query = {
      _id: roleId,
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.ROLES, query, update, {});
  }
  async roleDetails(params: IRoleDetails): Promise<any> {
    let { roleId } = params;
    let query = {
      _id: roleId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.ROLES, query, {}, {},{
      path: 'modules.moduleId',
      model: 'modules'
    });
  }
  async getAllRoles(params: IGetAllRoles): Promise<any> {
    let {search,limit=0,pageNo}=params;
    let query:any={
        status:{$ne:STATUS.DELETED}
    }
    if(search)
    query["$or"]= [
        { role: search }, 
        { description:search }
    ];

    let roles = await this.find(Models.ROLES, query, {}, {},{},{pageNo,limit},{
      path: 'modules.moduleId',
      model: 'modules'
    });
    let count = await this.count(Models.ROLES, query);
    return { roles, count };
  }
}
export const rolesDao = new RolesDao();
