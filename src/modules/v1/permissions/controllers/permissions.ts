import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import {
  checkModulePermissionExists,
  createModulePermissions,
  updateModulePermissions,
  getModulePermissionsDetails,
  getAllModulesPermissions,
} from "../services/permissions";
import { IModulesPermissions } from "../interfaces/permissions";
/**
 *
 * @param {*} params
 * @description for creating module permissions
 * @returns
 */
const addModulePermissions = asyncHandler(
  async (req: Request, res: Response) => {
    let param = req.body;
    await checkModulePermissionExists(param);
    await createModulePermissions({ ...param });
    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      MESSAGES.ADD_MODULE_PERMISSIONS_CREATE_SUCCESSFULLY,
      {}
    );
  }
);
/**
 *
 * @param {*} params
 * @description for edit module permission
 * @returns
 */
const editModulePermissions = asyncHandler(
  async (req: Request, res: Response) => {
    let param = req.body;
    await checkModulePermissionExists(param);

    await updateModulePermissions({ ...param });

    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      MESSAGES.MODULE_PERMISSIONS_UPDATED_SUCCESSFULLY,
      {}
    );
  }
);
/**
 *
 * @param {*} params
 * @description for module permission details
 * @returns
 */
const modulePermissionsDetails = asyncHandler(
  async (req: Request, res: Response) => {
    let { moduleId } = req.params;
    let permission = await getModulePermissionsDetails({ moduleId });
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, permission);
  }
);
/**
 *
 * @param {*} params
 * @description for modules permissions
 * @returns
 */
const modulesPermissions = asyncHandler(async (req: Request, res: Response) => {
  let params = req.query as unknown as IModulesPermissions;
  let permissions = await getAllModulesPermissions(params);
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, permissions);
});
export {
  addModulePermissions,
  editModulePermissions,
  modulePermissionsDetails,
  modulesPermissions,
};
