import MESSAGES from "../../../../messages/messages";
import { permissionDao } from "../dao/permissions";
import {
  ICheckModulePermissionExists,
  ICreateModulePermissions,
  IUpdateModulePermissions,
  IModulePermissionsDetails,
  IModulesPermissions
} from "../interfaces/permissions"
/**
 *
 * @param {*} params
 * @description for cheking module exists or not
 * @returns
 */
const checkModulePermissionExists = async (params: ICheckModulePermissionExists) => {
  const modulePermissionsExists =
    await permissionDao.checkModulePermissionExists(params);
  if (modulePermissionsExists)
    throw new Error(MESSAGES.MODULE_PERMISSIONS_ALREADY_EXISTS);
  return true;
};

/**
 *
 * @param {*} params
 * @description for create module permissions
 * @returns
 */
const createModulePermissions = async (params: ICreateModulePermissions) => {
  return await permissionDao.createPermissions(params);
};
/**
 *
 * @param {*} params
 * @description for update module permissions
 * @returns
 */
const updateModulePermissions = async (params: IUpdateModulePermissions) => {
  return await permissionDao.updatePermissions(params);
};
/**
 *
 * @param {*} params
 * @description for module permissions
 * @returns
 */
const getModulePermissionsDetails = async (params: IModulePermissionsDetails) => {
  return await permissionDao.getModulePermissionsDetails(params);
};
/**
 *
 * @param {*} params
 * @description for modules permissions
 * @returns
 */
const getAllModulesPermissions = async (params: IModulesPermissions) => {
  return await permissionDao.getAllModulesPermissions(params);
};
export {
  checkModulePermissionExists,
  createModulePermissions,
  updateModulePermissions,
  getModulePermissionsDetails,
  getAllModulesPermissions,
};
