export interface ISession{
    adminId?:string;
    userId?:string;
    deviceId?:string;
}
export interface IReadSession<T> {
    countSessions(ModelNames:ModelNames,params:any): Promise<number>;
}
export interface IWriteSession<T> {
    deleteActiveSessions(ModelNames:ModelNames,params:any): Promise<any>;
    activeSessions(ModelNames:ModelNames,params:any): Promise<any>;
    createSessions(ModelNames:ModelNames,params:any): Promise<any>;
}
