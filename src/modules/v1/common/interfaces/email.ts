export interface ISendEmailLink{
    email:string;
    adminId?:string;
    userId?:string;
    prefix?:string;
    subject?:string;
    content?:string;
    token?:string;
}

export interface IVerifyVerificationEmailLink{
    verificationEmailToken:string;
    prefix?:string;
}