import { RequestHandler, Request, Response, NextFunction } from "express";
import { getData } from "../../../../services/redis";
import SERVER from "../../../../config/environment";
import CONSTANT from "../../../../constant/constant";
import MESSAGES from "../../../../messages/messages";
import { STATUS } from "../../../../enums/enums";
import codes from "../../../../codes/status_codes";
import { jwtVerify } from "../../../../lib/universal-function";
import { CustomError } from "../dao/customError";

const adminAuthorization = async (req: any, res: any, next: NextFunction) => {
  try {
    let tokenData: any = req.headers;
    let ip = req.ip;
    const keys = [SERVER.API_KEY];
    const isKeyExits = keys.includes(tokenData?.apikey);
    const accessToken = tokenData?.authorization?.replace("Bearer ", "");
    if (accessToken) tokenData = await jwtVerify(accessToken);
    if (isKeyExits && tokenData?.deviceId) {
      let token = await getData(tokenData?.deviceId);
      if (token && token == accessToken) {
        tokenData ={...tokenData,ip}
        req.user = tokenData;
        next();
      } else
        throw new CustomError(MESSAGES.UNAUTHORIZED, {
          code: codes.UNAUTHORIZED,
        });
    } else
      throw new CustomError(MESSAGES.INVALID_SECRET_KEY, {
        code: codes.UNAUTHORIZED,
      });
  } catch (error) {
    next(error);
  }
};
const userAuthorization = async (req: any, res: any, next: NextFunction) => {
  try {
    let tokenData: any = req.headers;
    let ip = req.ip;
    const keys = [SERVER.API_KEY];
    const isKeyExits = keys.includes(tokenData?.apikey);
    const accessToken = tokenData?.authorization?.replace("Bearer ", "");
    if (accessToken) tokenData = await jwtVerify(accessToken);
    if (isKeyExits && tokenData?.deviceId) {
      let token = await getData(tokenData?.deviceId);
      if (token && token == accessToken) {
        tokenData ={...tokenData,ip}
        req.user = tokenData;
        next();
      } else
        throw new CustomError(MESSAGES.UNAUTHORIZED, {
          code: codes.UNAUTHORIZED,
        });
    } else
      throw new CustomError(MESSAGES.INVALID_SECRET_KEY, {
        code: codes.UNAUTHORIZED,
      });
  } catch (error) {
    next(error);
  }
};
export { adminAuthorization ,userAuthorization};
