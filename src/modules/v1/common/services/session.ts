import { deleteData, setData, expireKey } from "../../../../services/redis";
import { sessionDao } from "../dao/sessions";
import SERVER from "../../../../config/environment";
import CONSTANT from "../../../../constant/constant";
import MESSAGES from "../../../../messages/messages";
/**
 *
 * @param {*(session data)} params
 * @description for create session and validate login attempts
 * @returns
 */
const createSession = async (ModelName: ModelNames, params: TokenData) => {
    let { deviceId, accessToken } = params;
    let sessionCount = await sessionDao.countSessions(ModelName, params);
    if (sessionCount > CONSTANT.COUNTER.SESSION)
      throw new Error(MESSAGES.SESSION_LIMIT_EXCEEDED);
    if(deviceId && accessToken){
      await setData(deviceId, accessToken);
      await expireKey(deviceId, SERVER.JWT.EXPIRESIN?(parseInt(SERVER.JWT.EXPIRESIN)*1000):0);
    }
    await sessionDao.createSessions(ModelName, params);
    return true;
}
/**
 *
 * @param {deviceId,adminId/userId,isLogoutAllDevice} params
 * @description for delete sessions
 * @returns
 */
const deleteSessions = async (ModelName: ModelNames, params: TokenData) => {
  let { deviceId, adminId, userId, isLogoutAllDevice } = params;
  if (isLogoutAllDevice) {
    let sessions = await sessionDao.activeSessions(ModelName, {
      adminId,
      userId,
    });
    await sessionDao.deleteActiveSessions(ModelName, { deviceId });
    if (Array.isArray(sessions)) {
      for (let i = 0; i < sessions.length; i++) {
        await deleteData(sessions[i].deviceId);
      }
    }
  }
  else if (deviceId) {
    await deleteData(deviceId);
    await sessionDao.deleteActiveSessions(ModelName, { deviceId, userId });
  } 
  return true;
}
export {
    deleteSessions,
    createSession
};
