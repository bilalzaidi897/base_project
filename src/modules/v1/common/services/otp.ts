import CONSTANT from "../../../../constant/constant";
import { generateNumber, isJson } from "../../../../lib/universal-function";
import MESSAGES from "../../../../messages/messages";
import {
  getData,
  setData,
  deleteData,
  expireKey,
} from "../../../../services/redis";
import { sendSms } from "../../../../services/sms";
import {ISendOtp,IVerifyOtp} from "../interfaces/otp"
/**
 *
 * @param {phoneNo,countryCode,adminId,userId,prefix,message,otpCode} params
 * @description for send OTP
 * @returns
 */
const sendOtp=async(params: ISendOtp)=> {
  let { adminId, userId, phoneNo, countryCode, prefix, message, otpCode } =
    params;
  console.log("phoneNo",JSON.stringify(params))
  if (!prefix) prefix = CONSTANT.PREFIX.OTP;
  let count: number = 0;
  if (!otpCode) otpCode = await generateNumber();
  let key = prefix + otpCode;
  await setData(key, JSON.stringify({ adminId, userId, phoneNo }));
  await expireKey(key, CONSTANT.EXPIRE_TIME.OTP_EXPIRE_TIME);

  key = prefix + (adminId || userId);
  count = await getData(key);
  count = count || 0;
  if (count > CONSTANT.COUNTER.OTP)
    throw new Error(MESSAGES.OTP_LIMIT_EXCEEDED);

  await setData(key, (count++).toString());
  await expireKey(key, CONSTANT.EXPIRE_TIME.OTP_COUNTER_EXPIRE_TIME);
  await sendSms(countryCode + phoneNo, message);
}
/**
 *
 * @param {otpCode,deviceId,prefix,phoneNo} params
 * @description for verify OTP
 * @returns
 */
const verifyOtp=async(params: IVerifyOtp)=> {
  let { otpCode, deviceId, prefix, phoneNo } = params;
  if (!prefix) prefix = CONSTANT.PREFIX.OTP;
  let key = prefix + otpCode;
  let otpData = await getData(key);
  if (!otpData) throw new Error(MESSAGES.EXIPRE_OTP_CODE);

  otpData = await isJson(otpData);
  if (!otpData) throw new Error(MESSAGES.EXIPRE_OTP_CODE);

  if (otpData && otpData.phoneNo != phoneNo)
    throw new Error(MESSAGES.INVALID_OTP);
  deleteData(key);
  return otpData;
}

export {
    sendOtp,
    verifyOtp
}