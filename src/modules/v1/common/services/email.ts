import CONSTANT from "../../../../constant/constant";
import {
  isJson,
  randomString,
} from "../../../../lib/universal-function";
import MESSAGES from "../../../../messages/messages";
import {
  getData,
  setData,
  deleteData,
  expireKey,
} from "../../../../services/redis";
import { sendMail } from "../../../../services/mail";
import {ISendEmailLink,IVerifyVerificationEmailLink} from "../interfaces/email"
/**
 *
 * @param {email,adminId,userId,prefix,subject,content,token} params
 * @description for send verification email link
 * @returns
 */
const sendEmailLink=async (params: ISendEmailLink)=> {
  let { adminId, userId, email, prefix, subject, content, token } = params;
  let count: number = 0;
  if (!prefix) prefix = CONSTANT.PREFIX.EMAIL_VARIFICATION_LINK;
  if (!token) token = await randomString();
  let key = prefix + token;
  await setData(key, JSON.stringify({ userId,adminId,email }));
  await expireKey(key, CONSTANT.EXPIRE_TIME.EMAIL_LINK_EXPIRE_TIME);

  key = prefix + (adminId || userId);
  count = await getData(key);
  count = count || 0;
  if (count > CONSTANT.COUNTER.OTP)
    throw new Error(MESSAGES.EMAIL_VERIFICATION_LINK_LIMIT_EXCEEDED);

  await setData(key, (count++).toString());
  await expireKey(key, CONSTANT.EXPIRE_TIME.EMAIL_COUNTER_LINK_EXPIRE_TIME);
  await sendMail({ email, subject, content });
  return token;
}
/**
 *
 * @param {verificationEmailToken,prefix} params
 * @description for verify verification email link
 * @returns
 */
const verifyVerificationEmailLink=async (params: IVerifyVerificationEmailLink) =>{
  let { verificationEmailToken, prefix } = params;
  if (!prefix) prefix = CONSTANT.PREFIX.EMAIL_VARIFICATION_LINK;
  let key = prefix + verificationEmailToken;
  let emailData = await getData(key);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);

  emailData = await isJson(emailData);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);
  deleteData(key);
  return emailData;
}

export {
    sendEmailLink,
    verifyVerificationEmailLink
}