"use strict";

import { QueryOptions } from "mongoose";
import * as models from "../../../../models";
import { isEmpty } from "../../../../lib/universal-function";
export class BaseDao {
  BaseDao: any;
  async save(model: ModelNames, data: any) {
    try {
      const ModelName: any = models[model];
      return await new ModelName(data).save();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async find(
    model: ModelNames,
    query: any,
    projection: any,
    options: QueryOptions,
    sort?: any,
    paginate?: any,
    populateQuery?: any
  ) {
    try {
      const ModelName: any = models[model];
      if (!isEmpty(sort) && !isEmpty(paginate) && isEmpty(populateQuery)) {
        // sorting with pagination
        return await ModelName.find(query, projection, options)
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit)
          .sort(sort);
      } else if (
        isEmpty(sort) &&
        !isEmpty(paginate) &&
        isEmpty(populateQuery)
      ) {
        // pagination
        return await ModelName.find(query, projection, options)
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit);
      } else if (
        isEmpty(sort) &&
        isEmpty(paginate) &&
        !isEmpty(populateQuery)
      ) {
        // populate
        return await ModelName.find(query, projection, options)
          .populate(populateQuery)
          .exec();
      } else if (
        isEmpty(sort) &&
        !isEmpty(paginate) &&
        !isEmpty(populateQuery)
      ) {
        // pagination with populate
        return await ModelName.find(query, projection, options)
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit)
          .populate(populateQuery)
          .exec();
      }else if (
        !isEmpty(sort) &&
        !isEmpty(paginate) &&
        !isEmpty(populateQuery)
      ) {
        // pagination with populate
        return await ModelName.find(query, projection, options)
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit)
          .sort(sort)
          .populate(populateQuery)
          .exec();
      } else {
        return await ModelName.find(query, projection, options);
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async distinct(model: ModelNames, path: string, query: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.distinct(path, query);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne(
    model: ModelNames,
    query: any,
    projection: any,
    options: any,
    populateQuery: any
  ) {
    try {
      const ModelName: any = models[model];
      if (!isEmpty(populateQuery)) {
        // populate
        return await ModelName.findOne(query, projection, options)
          .populate(populateQuery)
          .exec();
      } else {
        return await ModelName.findOne(query, projection, options);
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOneAndUpdate(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.findOneAndUpdate(query, update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findAndRemove(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.findOneAndRemove(query, update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async update(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.update(query, update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.updateOne(query, update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateMany(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.updateMany(query, update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async remove(model: ModelNames, query: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.remove(query);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteMany(model: ModelNames, query: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.deleteMany(query);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async count(model: ModelNames, query: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.countDocuments(query);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async countDocuments(model: ModelNames, query: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.countDocuments(query);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async aggregate(model: ModelNames, aggPipe: any, options: QueryOptions) {
    try {
      const ModelName: any = models[model];
      const aggregation: any = ModelName.aggregate(aggPipe);
      if (options) {
        aggregation.options = options;
      }
      return await aggregation.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async insert(model: ModelNames, data: any, options: QueryOptions) {
    try {
      const ModelName: any = models[model];
      const obj = new ModelName(data);
      await obj.save();
      return obj;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async insertMany(model: ModelNames, data: any, options: QueryOptions) {
    try {
      const ModelName: any = models[model];
      return await ModelName.collection.insertMany(data, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async aggregateDataWithPopulate(
    model: ModelNames,
    group: any,
    populateOptions: any
  ) {
    try {
      const ModelName: any = models[model];
      const aggregate = await ModelName.aggregate(group);
      const populate = await ModelName.populate(aggregate, populateOptions);
      return populate;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async bulkFindAndUpdate(
    bulk: any,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      return await bulk.find(query).upsert().update(update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async bulkFindAndUpdateOne(
    bulk: any,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      return await bulk.find(query).upsert().updateOne(update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findByIdAndUpdate(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.findByIdAndUpdate(query, update, options);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async populate(model: ModelNames, data: any, populateQuery: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.populate(data, populateQuery);
    } catch (error) {
      return Promise.reject(error);
    }
  }
  async _addSkipLimit(limit: number, pageNo: number) {
    if (limit) {
      limit = Math.abs(limit);
      // If limit exceeds max limit
      if (limit > 100) {
        limit = 100;
      }
    } else {
      limit = 10;
    }
    if (pageNo && pageNo !== 0) {
      pageNo = Math.abs(pageNo);
    } else {
      pageNo = 1;
    }
    let skip = limit * (pageNo - 1);
    return [{ $skip: skip }, { $limit: limit + 1 }];
  }
  _queryBuilder(
    pipeline?: Array<Object>,
    skip?: number,
    limit?: number,
    pageNo?: number
  ): any[] {
    const query = pipeline || [];

    query.push({
      $facet: {
        data: [{ $skip: skip }, { $limit: limit }],
        metadata: [{ $count: "total" }, { $addFields: { page: pageNo } }],
      },
    });
    return query;
  }
  async _paginate(
    model: ModelNames,
    pipeline?: Array<Object>,
    limit?: number,
    pageNo?: number
  ) {
    try {
      const ModelName = models[model];

      if (limit) {
        limit = Math.abs(limit);

        // If limit exceeds max limit
        if (limit > 300) {
          limit = 300;
        }
      } else {
        limit = 10;
      }
      if (pageNo && pageNo !== 0) {
        pageNo = Math.abs(pageNo);
      } else {
        pageNo = 1;
      }
      const skip: number = limit * (pageNo - 1);
      const result = await ModelName.aggregate(
        this._queryBuilder(pipeline, skip, limit, pageNo)
      ).exec();

      let next_hit = 0;
      let total_page =
        result[0]["data"].length > 0
          ? Math.ceil(result[0].metadata[0].total / limit)
          : 0;
      if (result[0]["data"].length > limit) {
        result[0]["data"].pop();
      }

      if (total_page > pageNo) {
        next_hit = pageNo + 1;
      }

      return {
        total:
          result[0]["metadata"] && result[0]["metadata"][0]
            ? result[0]["metadata"][0]["total"]
            : 0,
        pageNo:
          result[0]["metadata"] && result[0]["metadata"][0]
            ? result[0]["metadata"][0]["page"]
            : pageNo,
        totalPage: total_page,
        nextHit: next_hit,
        limit: limit,
        data: result[0]["data"],
        filterCount: result[0]["data"].length,
      };
    } catch (error) {
      throw new Error(JSON.stringify(error));
    }
  }
  async aggregateStream(model: ModelNames, aggPipe: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.aggregate(aggPipe)
        .cursor({ batchSize: 100 })
        .exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }
  async findStream(
    model: ModelNames,
    query: any,
    projection: any,
    options: QueryOptions,
    sort: any
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.find(query, projection, options)
        .sort(sort)
        .stream();
    } catch (error) {
      return Promise.reject(error);
    }
  }
  async getColl(model: ModelNames) {
		try {
			const ModelName: any = models[model];
			const changeStream = await ModelName.watch({fullDocument: "updateLookup"});
			changeStream.on("change", (error:any, data:any) => {
				console.log(data);
			});
			return changeStream;
		} catch (error) {
			return Promise.reject(error);
		}
	}
}

export const baseDao = new BaseDao();
