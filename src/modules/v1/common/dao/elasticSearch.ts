"use strict";

import { Client } from "@elastic/elasticsearch";
import SERVER from "../../../../config/environment";
import CONSTANT from "../../../../constant/constant";
import logger from "../../../../services/logger";
import {
  ElasticSearchMapping,
  ElasticSearchDeleteIndex,
  ElasticSearchCreateIndex,
} from "../interfaces/elasticSearch";
import ELASTIC_MAPPING_MODEL from "../../../../elasticSearch/models";
// instantiate an Elasticsearch this.client

export class ElasticSearch {
  client;
  constructor() {
    this.client = new Client({
      node: SERVER.ELASTIC_SEARCH.HOST,
      //  cloud: { id: '<cloud-id>' },
      //auth: { apiKey: 'base64EncodedKey' }
    });
  }
  async init() {
    // ping the this.client to be sure Elasticsearch is up
    this.client
      .ping()
      .then((response) => {
        console.log("You are connected to Elasticsearch!");
        logger.info("You are connected to Elasticsearch!");
      })
      .catch((error) => {
        console.error("Elasticsearch is not connected.");
        logger.warn("Elasticsearch is not connected.");
        throw error;
      });
      await this.elasticCreateIndex({indexName:CONSTANT.ELASTIC_SEARCH_INDEXES.CATALOGUES});
      await this.elasticMapping({
        mappingkey:"catalogues",
        indexName:CONSTANT.ELASTIC_SEARCH_INDEXES.CATALOGUES
      })
  }
  // Create Index
  async createIndex(indexName: string) {
    return await this.client.indices.create({ index: indexName });
  }

  // Check if index exists
  async indexExists(indexName: string) {
    return await this.client.indices.exists({ index: indexName });
  }

  // Preparing index and its mapping
  async initMapping(indexName: string, payload: any) {
    return await this.client.indices.putMapping({
      index: indexName,
      body: payload,
    });
  }

  /**
   * @description add a data to the index that has already been created
   * @note if the id key is omitted, Elasticsearch will auto-generate one.
   */
  // Add/Update a document
  async addDocument(indexName: string, _id: string, payload: any) {
    return await this.client.index({
      index: indexName,
      id: String(_id),
      body: payload,
    });
  }

  // Update a document
  async updateDocument(
    indexName: string,
    _id: string,
    docType: string,
    payload: any
  ) {
    return await this.client.update({
      index: indexName,
      id: String(_id),
      body: { doc: payload },
      //conflicts:"proceed"
    });
  }
  // Update a document
  async updateDocumentByQuery(indexName: string, payload: any) {
    return await this.client.updateByQuery({
      index: indexName,
      refresh: true,
      body: payload,
      conflicts: "proceed",
    });
  }

  // Search
  async search(indexName: string, payload: any) {
    return await this.client.search({
      index: indexName,
      body: payload,
    });
  }

  // Search using DSL
  async searchWithDSL(indexName: string, docType: string, payload: any) {
    return await this.client.search({
      index: indexName,
      body: payload,
    });
  }

  // Delete a document from an index
  async deleteDocument(indexName: string, id: string) {
    return await this.client.delete({
      index: indexName,
      id,
    });
  }
  // delete documents by DSL
  async deleteWithDSL(indexName: string, payload: any) {
    return await this.client.deleteByQuery({
      index: indexName,
      body: payload,
      conflicts: "proceed",
    });
  }

  // Delete all
  async deleteAll(indexName: string) {
    return await this.client.indices.delete({
      index: indexName,
    });
  }

  /**
   * @description perform bulk indexing of the data passed
   */
  async bulkAddDocument(indexName: string, bulk: any) {
    return await this.client.bulk({
      index: indexName,
      body: bulk,
    });
  }
  /**
   * @function elasticMapping all mapping
   * @description elasticMapping all mapping
   */
  async elasticMapping(params: ElasticSearchMapping) {
    let { indexName, mappingkey } = params;
    let ELASTIC_MAPPING: any = ELASTIC_MAPPING_MODEL;
    let checkIndex: any = null;
    if (indexName && ELASTIC_MAPPING[mappingkey]) {
      checkIndex = await this.indexExists(indexName);
      if (checkIndex)
        return this.initMapping(indexName, ELASTIC_MAPPING[mappingkey]);
      else false;
    } else return false;
  }
  /**
   * @function elasticDeleteIndex all delete
   * @description elasticDeleteIndex all delete
   */
  async elasticDeleteIndex(params: ElasticSearchDeleteIndex) {
    let { indexName } = params;
    let checkIndex: any = null;
    if (indexName) {
      checkIndex = await this.indexExists(indexName);
      if (checkIndex) return this.deleteAll(indexName);
      else return true;
    }
  }
  /**
   * @function elasticCreateIndex all delete
   * @description elasticCreateIndex all delete
   */
  async elasticCreateIndex(params: ElasticSearchCreateIndex) {
    let { indexName } = params;
    let checkIndex: any = null;
    console.log("indexName",indexName)
    if (indexName) {
      checkIndex = await this.indexExists(indexName);
      if (!checkIndex) return await this.createIndex(indexName);
      return true;
    }
  }
}

export const elasticSearch = new ElasticSearch();
