import { IReadSession, IWriteSession,ISession } from "../interfaces/sessions";
import { BaseDao } from "./baseDao";
import { STATUS } from "../../../../enums/enums";
class SessionDao<T> extends BaseDao implements IReadSession<T>, IWriteSession<T> {
  /**
   *
   * @param {*} params
   * @description for delete active sessions
   * @returns
   */
  async deleteActiveSessions(
    ModelNames: ModelNames,
    params: ISession
  ): Promise<any> {
    let { adminId, userId, deviceId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED }
    };
    if (adminId) query.adminId = adminId;
    if (userId) query.userId = userId;
    if (deviceId) query.deviceId = deviceId;
    return await this.update(ModelNames, query,{status:STATUS.DELETED},{multi:true});
  }
  /**
   *
   * @param {*} params
   * @description for active sessions
   * @returns
   */
  async activeSessions(ModelNames: ModelNames, params: ISession): Promise<any> {
    let { adminId, userId, deviceId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (adminId) query.adminId = adminId;
    if (userId) query.userId = userId;
    if (deviceId) query.deviceId = deviceId;
    return await this.find(ModelNames, query, {}, {});
  }
  /**
   *
   * @param {*} params
   * @description for count sessions
   * @returns
   */
  async countSessions(ModelNames: ModelNames, params: ISession): Promise<number> {
    let { adminId, userId } = params;
    let query :any= {
      status: { $ne: STATUS.DELETED },
    }
    if (adminId) query.adminId = adminId;
    if (userId) query.userId = userId;
    return await this.count(ModelNames, query);
  }
  /**
   *
   * @param {*} params
   * @description for create sessions
   * @returns
   */
  async createSessions(ModelNames: ModelNames, params: any): Promise<any> {
    return await this.save(ModelNames, params);
  }
}
export const sessionDao = new SessionDao();
