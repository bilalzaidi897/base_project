import {ICustomError} from "../interfaces/customError"
import codes from "../../../../codes/status_codes"
export class CustomError extends Error {
  code:number;
  constructor(message: string,options?: ICustomError) {
    super(message);
    this.code = options?.code || codes.BAD_REQUEST;
    this.message=message;
  }
  getErrorMessage() {
    return this.message;
  }
}