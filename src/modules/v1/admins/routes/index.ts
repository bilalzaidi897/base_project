import { Router } from "express";
const router = Router();
import {
  createAdminUser,
  login,
  logout,
  profile,
  adminProfile,
  updateProfile,
  changePassword,
  sendOtp,
  verifyOtp,
  sendVerifyEmail,
  verifyEmail,
  forgotPassword,
  resetPassword,
  uploadDoc,
  uploadProfileImage,
  getProfiles
} from "../controllers/admins";
import {
  validateAddAdmin,
  validateLogin,
  validateLogout,
  validateProfile,
  validateAdminProfile,
  validateUpdateProfile,
  validateChangePassword,
  validateSendOtp,
  validateVerifyOtp,
  validateSendVerifyEmail,
  validateVerifyEmail,
  validateForgotPassword,
  validateResetPassword,
  validateUploadDoc,
  validateUploadProfileImage,
  validateAdmins
} from "../validaters/admins";
import { adminAuthorization } from "../../common/services/authorization";

router.post("/",validateAddAdmin,adminAuthorization, createAdminUser);
router.put("/login", validateLogin, login);
router.put("/logout", validateLogout, adminAuthorization, logout);
router.get("/profile/:adminId", validateAdminProfile, adminAuthorization, adminProfile);
router.get("/profile", validateProfile, adminAuthorization, profile);
router.put("/", validateUpdateProfile, adminAuthorization, updateProfile);
router.put(
  "/changePassword",
  validateChangePassword,
  adminAuthorization,
  changePassword
);
router.put(
  "/changePhoneNo/sendOtp",
  validateSendOtp,
  adminAuthorization,
  sendOtp
);
router.put(
  "/changePhoneNo/verifyOtp",
  validateVerifyOtp,
  verifyOtp
);
router.put(
  "/changeEmail/sendVerifyEmail",
  validateSendVerifyEmail,
  adminAuthorization,
  sendVerifyEmail
);
router.put("/changeEmail/verifyEmail", validateVerifyEmail, verifyEmail);
router.put(
  "/forgotPassword",
  validateForgotPassword,
  forgotPassword
);
router.put("/resetPassword", validateResetPassword, resetPassword);

/*
FILE UPLOAD API'S
*/
router.post("/upload/doc", validateUploadDoc, adminAuthorization, uploadDoc);
router.post(
  "/upload/profile",
  validateUploadProfileImage,
  adminAuthorization,
  uploadProfileImage
); 
router.get(
  "/",
  validateAdmins,
  adminAuthorization,
  getProfiles
);
export default router;
