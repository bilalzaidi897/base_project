import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import CONSTANT from "../../../../constant/constant";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import {
  sendResponse,
  isJson,
  delay,
  getWhatsAppDate
} from "../../../../lib/universal-function";
import {
  availabilityCheckEmailPhoneNo,
  checkAdminExists,
  checkParentAdminExistsAndAdminType,
  createAdmin,
  genrateAdminAuthToken,
  adminSendOtp,
  adminVerifyOtp,
  adminSendEmailLink,
  adminVerifyVerificationEmailLink,
  profiles,
} from "../services/admins";
import { deleteSessions } from "../../common/services/session";
import { adminDao } from "../dao/admins";
import { STATUS } from "../../../../enums/enums";
import {
  uploadImageToS3Bucket,
  uploadOriginalImageToS3Bucket,
} from "../../../../services/s3";
import { getData, deleteData } from "../../../../services/redis";
import { IProfiles } from "../interfaces/admins";

/**
 *
 * @param {*} params
 * @description for add admin
 * @returns
 */
const createAdminUser = asyncHandler(async (req: Request, res: Response) => {
  let { deviceId, adminId, parentId, ip, deviceType, deviceName }: any =
    req.user;
  let params = req.body;
  let { email, phoneNo, password } = params;
  if (parentId) await checkParentAdminExistsAndAdminType(parentId);
  await availabilityCheckEmailPhoneNo({ email,phoneNo, isSignUpRequest: true });
  params.status = STATUS.PENDING;
  const doc = await createAdmin(params);
  await doc.setPassword(password);
  await doc.save();
  let admin = doc.toJSON();
  let tokenData = await genrateAdminAuthToken({
    ...admin,
    ip,
    deviceType,
    deviceName,
  });
  admin = { ...admin, ...tokenData };
  // if (!admin.isPhoneNoVerified) {
  //   adminSendOtp({...admin});
  // }
  // if (!admin.isEmailVerified) {
  //   adminSendEmailLink(admin);
  // }
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SUCCESSFULLY_REGISTERED,
    admin
  );
});
/**
 *
 * @param {*} params
 * @description for login
 * @returns
 */
const login = asyncHandler(async (req: Request, res: Response) => {
  let { devicetype: deviceType, deviceName: deviceName } = req.headers;
  let ip = req.ip;
  let { email, password } = req.body;
  let admin = await checkAdminExists({ email });
  await admin.authenticate(password);
  admin = admin.toJSON();
  let tokenData = await genrateAdminAuthToken({
    ...admin,
    ip,
    deviceType,
    deviceName,
  });
  admin = { ...admin, ...tokenData };
  // if (!admin.isPhoneNoVerified) {
  //   adminSendOtp({...admin});
  // }
  // if (!admin.isEmailVerified) {
  //   adminSendEmailLink(admin);
  // }
  // await delay(3000);
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
const logout = asyncHandler(async (req: Request, res: Response) => {
  let { deviceId, adminId }: any = req.user;
  await deleteSessions(Models.ADMIN_SESSIONS, {
    deviceId,
    adminId,
    ...req.body,
  });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.LOGOUT_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for admin profile
 * @returns
 */
const adminProfile = asyncHandler(async (req: Request, res: Response) => {
  let { adminId } = req.params;
  let admin = await adminDao.adminDetails({ adminId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
/**
 *
 * @param {*} params
 * @description for profile
 * @returns
 */
const profile = asyncHandler(async (req: Request, res: Response) => {
  let { adminId }: any = req.user;
  let admin = await adminDao.adminDetails({ adminId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
/**
 *
 * @param {*} params
 * @description for update profile
 * @returns
 */
const updateProfile = asyncHandler(async (req: Request, res: Response) => {
  let params = req.body;
  let { adminId } = params;
  let admin = await adminDao.updateAdminDetails({
    ...params,
    adminId,
  });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.PROFILE_UPDATED, admin);
});
/**
 *
 * @param {*} params
 * @description for change password
 * @returns
 */
const changePassword = asyncHandler(async (req: Request, res: Response) => {
  let { adminId, oldPassword, newPassword } = req.body;
  const admin = await adminDao.adminDetails({ adminId });
  await admin.authenticate(oldPassword);
  await admin.setPassword(newPassword);
  await admin.save();
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for send OTP
 * @returns
 */
const sendOtp = asyncHandler(async (req: Request, res: Response) => {
  let { phoneNo, countryCode }: any = req.body;
  let { adminId }: any = req.user;
  await availabilityCheckEmailPhoneNo({
    adminId,
    phoneNo,
    isSignUpRequest: true,
  });
  await adminSendOtp({ adminId, phoneNo: phoneNo, countryCode: countryCode });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SEND_OTP_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for verify OTP
 * @returns
 */
const verifyOtp = asyncHandler(async (req: Request, res: Response) => {
  let { otpCode, phoneNo, countryCode } = req.body;
  const admin = await adminVerifyOtp({ otpCode, phoneNo });
  let update: any = {
    isPhoneNoVerified: true,
    phoneNo,
    countryCode,
  };
  if (admin.isEmailVerified) update.status = STATUS.ACTIVE;
  await availabilityCheckEmailPhoneNo({
    adminId: admin?.adminId,
    phoneNo,
    isSignUpRequest: true,
  });
  await adminDao.updateAdminDetails({ ...update, adminId: admin?.adminId });
  if (admin.isEmailVerified)
    await deleteSessions(Models.ADMIN_SESSIONS, {
      adminId: admin?.adminId,
      isLogoutAllDevice: true,
    });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.OTP_VERIFIED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for send verify email
 * @returns
 */
const sendVerifyEmail = asyncHandler(async (req: Request, res: Response) => {
  let { adminId }: any = req.user;
  let admin = await adminDao.adminDetails({ adminId });
  admin = admin.toJSON();
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  if (admin.status == STATUS.IN_ACTIVATED)
    throw new Error(MESSAGES.USER_IS_BLOCKED);
  if (!admin.isEmailVerified && admin?.email) {
    adminSendEmailLink({ adminId: adminId, email: admin?.email });
  }
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SEND_EMAIL_VERIFICATION_LINK,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for verify email
 * @returns
 */
const verifyEmail = asyncHandler(async (req: Request, res: Response) => {
  let { verificationEmailToken } = req.body;
  let emailVerifyData = await adminVerifyVerificationEmailLink({
    verificationEmailToken,
  });
  if (!emailVerifyData || (emailVerifyData && !emailVerifyData.adminId))
    throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);
  let admin = await adminDao.adminDetails({
    adminId: emailVerifyData.adminId,
  });
  admin = admin.toJSON();
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  let update: any = {
    isEmailVerified: true,
  };
  if (admin.isPhoneNoVerified) {
    update.status = STATUS.ACTIVE;
    await deleteSessions(Models.ADMIN_SESSIONS, {
      adminId: admin.adminId,
      isLogoutAllDevice: true,
    });
  }
  await adminDao.updateAdminDetails({
    ...update,
    adminId: admin.adminId,
  });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.EMAIL_VERIFIED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for forgot password
 * @returns
 */
const forgotPassword = asyncHandler(async (req: Request, res: Response) => {
  let { email } = req.body;
  let prefix = CONSTANT.PREFIX.FORGOT_PASSWORD;
  let admin = await checkAdminExists({ email });
  admin = admin.toJSON();
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  adminSendEmailLink({ ...admin, prefix });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.FORGOT_PASSWORD_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for reset password
 * @returns
 */
const resetPassword = asyncHandler(async (req: Request, res: Response) => {
  let { passwordResetToken, newPassword } = req.body;
  let prefix = CONSTANT.PREFIX.FORGOT_PASSWORD;
  let key = prefix + passwordResetToken;
  let emailData = await getData(key);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_RESET_PASSWORD_LINK);

  emailData = await isJson(emailData);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_RESET_PASSWORD_LINK);
  deleteData(key);
  let { adminId } = emailData;
  const admin = await adminDao.adminDetails({ adminId });
  await admin.setPassword(newPassword);
  await admin.save();
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for upload Doc
 * @returns
 */
const uploadDoc = asyncHandler(async (req: any, res: Response) => {
  let s3File: any = {};
  if (req.files && req.files.docFile && req.files.docFile.size > 0) {
    s3File = await uploadOriginalImageToS3Bucket(
      req.files.docFile,
      CONSTANT.S3_FOLDERS.ADMINS
    );
  }
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, s3File);
});
/**
 *
 * @param {*} params
 * @description for upload profile image
 * @returns
 */
const uploadProfileImage = asyncHandler(async (req: any, res: Response) => {
  let data: any = {};
  if (
    req.files &&
    req.files.image &&
    req.files.image.size &&
    req.files.image.size < 3072000
  ) {
    data = await uploadImageToS3Bucket(
      req.files.image,
      CONSTANT.S3_FOLDERS.ADMINS
    );
  }
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, data);
});
/**
 *
 * @param {*} params
 * @description for profiles
 * @returns
 */
const getProfiles = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query as unknown as IProfiles;
  let profilesData = await profiles({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    profiles: profilesData?.profiles || [],
    count: profilesData?.count || 0,
  });
});
export {
  createAdminUser,
  login,
  logout,
  profile,
  adminProfile,
  updateProfile,
  changePassword,
  sendOtp,
  verifyOtp,
  sendVerifyEmail,
  verifyEmail,
  forgotPassword,
  resetPassword,
  uploadDoc,
  uploadProfileImage,
  getProfiles,
};
