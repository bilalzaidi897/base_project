import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  IWriteAdmin,
  IReadAdmin,
  ICheckAdminExists,
  IProfiles
} from "../interfaces/admins";
export class AdminDao<T>
  extends BaseDao
  implements IReadAdmin<T>, IWriteAdmin<T>
{
  async checkAdminExists(
    params: ICheckAdminExists
  ): Promise<any> {
    let { email, phoneNo, adminId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    query["$or"]=[];
    if (email) query["$or"].push({email:email});
    if (phoneNo) query["$or"].push({phoneNo:phoneNo});
    if (adminId) query._id = { $ne: Object(adminId) };
    return await this.findOne(Models.ADMIN, query, {}, {}, {});
  }
  async adminDetails(params: any): Promise<any> {
    let { adminId } = params;
    let query = {
      _id: adminId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.ADMIN, query, {}, {}, {});
  }
  async createAdminUser(params: any): Promise<any> {
    return await this.save(Models.ADMIN,params);
  }
  async updateAdminDetails(params: any): Promise<any> {
    let { adminId } = params;
    let query = {
      _id: adminId,
      status: { $ne: STATUS.DELETED },
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.ADMIN, query, update, {});
  }
  async profiles(params: IProfiles): Promise<any> {
    let { search, limit = 0, pageNo } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (search) query["$or"] = [{ firstName: search },{email:search}, { phoneNo: search }];

    let profilesData = await this.find(
      Models.ADMIN,
      query,
      {},
      {},
      {},
      { pageNo, limit },
      {}
    );
    let count = await this.count(Models.ADMIN, query);
    return { profiles: profilesData, count };
  }
}
export const adminDao = new AdminDao();
