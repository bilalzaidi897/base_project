import SERVER from "../../../../config/environment";
import CONSTANT from "../../../../constant/constant";
import MESSAGES from "../../../../messages/messages";
import { ADMIN_TYPE, STATUS } from "../../../../enums/enums";
import {
  generateNumber,
  renderMessage,
  randomString,
  readFile,
  isJson,
} from "../../../../lib/universal-function";
import { getData, deleteData } from "../../../../services/redis";
import { genrateAuthToken } from "../../common/services/auth";
import { ISendOtp } from "../../common/interfaces/otp";
import { sendOtp } from "../../common/services/otp";
import { ISendEmailLink } from "../../common/interfaces/email";
import { sendEmailLink } from "../../common/services/email";
import {
  ICheckAdminExists,
  IVerifyVerificationEmailLink,
  IVerifyOtp,
  IProfiles
} from "../interfaces/admins";
import { adminDao } from "../dao/admins";

const availabilityCheckEmailPhoneNo = async (param: ICheckAdminExists) => {
  let { email,phoneNo,isSignUpRequest,adminId } = param;
  let admin = await adminDao.checkAdminExists({adminId, email,phoneNo });
  if (!admin) return true
  if (admin && email && admin.email==email && isSignUpRequest) throw new Error(MESSAGES.EMAIL_ALREDAY_EXIT);
  else if (admin && phoneNo && admin.phoneNo==phoneNo && isSignUpRequest) throw new Error(MESSAGES.PHONE_NUMBER_ALREADY_EXISTS);
  return admin;
};
const checkAdminExists = async (param: ICheckAdminExists) => {
  let { email,phoneNo,isSignUpRequest,adminId } = param;
  let admin = await adminDao.checkAdminExists({adminId, email,phoneNo });
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  if (admin && email && isSignUpRequest) throw new Error(MESSAGES.EMAIL_ALREDAY_EXIT);
  else if (admin && phoneNo && isSignUpRequest) throw new Error(MESSAGES.PHONE_NUMBER_ALREADY_EXISTS);
  if (admin.status == STATUS.IN_ACTIVATED) throw new Error(MESSAGES.USER_IS_BLOCKED);
  return admin;
};
const checkParentAdminExistsAndAdminType = async (parentId: string) => {
   let parentAdminData = await adminDao.adminDetails({ parentId });
    if(!parentAdminData) throw new Error(MESSAGES.USER_NOT_FOUND);
    else if(parentAdminData && [ADMIN_TYPE.ADMIN_USER,ADMIN_TYPE.COMPANY_USER].includes(parentAdminData?.adminType)){
      throw new Error(MESSAGES.INVALID_ADMIN_TYPE_SELECT);
    }
    return parentAdminData;
}
/**
 * 
 * @param {*} params 
 * @description for create admin
 * @returns 
 */
const createAdmin=async(params:any)=> {
  return await adminDao.createAdminUser(params);
}
const genrateAdminAuthToken = async (admin: TokenData) => {
  const deviceId = admin.adminId + "-" + new Date().getTime();
  let accessToken = await genrateAuthToken(Models.ADMIN_SESSIONS, {
    ...admin,
    deviceId,
  });
  return { deviceId, accessToken };
};
const adminSendOtp = async (admin: ISendOtp) => {
  let otpCode = await generateNumber();
  let message = await renderMessage(CONSTANT.NOTIFICATIONS.OTP.MESSAGE, {
    otpCode,
  });
  sendOtp({ ...admin, message, otpCode });
};
/**
 *
 * @param {otpCode,prefix,phoneNo} params
 * @description for verify OTP
 * @returns
 */
async function adminVerifyOtp(params: IVerifyOtp) {
  let { otpCode, prefix, phoneNo } = params;
  if (!prefix) prefix = CONSTANT.PREFIX.OTP;
  let key = prefix + otpCode;
  let otpData:any = await getData(key);
  if (!otpData) throw new Error(MESSAGES.EXIPRE_OTP_CODE);

  otpData = await isJson(otpData);
  if (!otpData) throw new Error(MESSAGES.EXIPRE_OTP_CODE);
  if (otpData && otpData.phoneNo != phoneNo)
    throw new Error(MESSAGES.INVALID_OTP);
  deleteData(key);
  let admin = await adminDao.adminDetails({ adminId:otpData?.adminId });
  admin = admin.toJSON();
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  return admin;
}
/**
 *
 * @param {email,adminId,prefix,subject,content,token} params
 * @description for send verification email link
 * @returns
 */
const adminSendEmailLink = async (admin: ISendEmailLink) => {
  let verificationEmailToken = await randomString();
  let subject = await renderMessage(
    CONSTANT.NOTIFICATIONS.ADMIN_EMAIL_VARIFICATION_LINK.SUBJECT,
    {}
  );
  let content = await readFile(
    CONSTANT.NOTIFICATIONS.ADMIN_EMAIL_VARIFICATION_LINK.PATH
  );
  let verificationLink =
    SERVER.ADMIN_EMAIL_VERIFICATION_PAGE +
    "?verificationEmailToken=" +
    verificationEmailToken;
  content = await renderMessage(content, { verificationLink });
  sendEmailLink({
    ...admin,
    subject,
    content,
    token: verificationEmailToken,
  });
};
/**
 *
 * @param {verificationEmailToken,prefix} params
 * @description for verify verification email link
 * @returns
 */
async function adminVerifyVerificationEmailLink(
  params: IVerifyVerificationEmailLink
) {
  let { verificationEmailToken, prefix } = params;
  if (!prefix) prefix = CONSTANT.PREFIX.EMAIL_VARIFICATION_LINK;
  let key = prefix + verificationEmailToken;
  let emailData = await getData(key);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);
  emailData = await isJson(emailData);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);
  deleteData(key);
  return emailData;
}
/**
 *
 * @param {*} params
 * @description for profiles
 * @returns
 */
const profiles = async (params: IProfiles) => {
  return await adminDao.profiles(params);
};
export {
  availabilityCheckEmailPhoneNo,
  checkAdminExists,
  checkParentAdminExistsAndAdminType,
  createAdmin,
  genrateAdminAuthToken,
  adminSendOtp,
  adminVerifyOtp,
  adminSendEmailLink,
  adminVerifyVerificationEmailLink,
  profiles
};
