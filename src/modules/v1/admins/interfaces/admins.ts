export interface ICheckAdminExists{
    email?:string;
    phoneNo?:string;
    isSignUpRequest?:boolean;
    adminId?:string;
}
export interface IVerifyOtp{
    otpCode?:string;
    phoneNo?:string;
    prefix?:string;
}
export interface IVerifyVerificationEmailLink{
    verificationEmailToken?:string;
    prefix?:string;
}
export interface IProfiles{
    pageNo?:number;
    limit?:number;
    search?:string;
}
export interface IReadAdmin<T> {
    checkAdminExists(params:any): Promise<any>;
    adminDetails(params:any): Promise<any>;
    profiles(params:IProfiles):Promise<any>;
}
export interface IWriteAdmin<T> {
    createAdminUser(params:any): Promise<any>;
    updateAdminDetails(params:any): Promise<any>;
}

