import { Router } from 'express';
const router = Router();
import adminRoutes from "./admins/routes";
import catalogueRoutes from "./catalogues/routes";
import moduleRoutes from "./modules/routes";
import permissionsRoutes from "./permissions/routes";
import roleRoutes from "./roles/routes";
import userRoutes from "./users/routes";
router.use("/admins", adminRoutes);
router.use("/catalogues", catalogueRoutes);
router.use("/modules", moduleRoutes);
router.use("/permissions", permissionsRoutes);
router.use("/roles", roleRoutes);
router.use("/users", userRoutes);

export default router;
