import {
    RequestHandler,
    Request,
    Response,
    NextFunction,
    query,
  } from "express";
import joi from "joi"
import { validateSchema } from "../../../../lib/universal-function";
/**
 *
 * @param {*} params
 * @description for register
 * @returns
 */
const validateSignUp = async (req:Request, res:Response, next:NextFunction) => {
    try {
      let schema = joi.object().keys({
        firstName: joi
          .string()
          .regex(/^[a-zA-Z ]+$/)
          .optional(),
        lastName: joi
          .string()
          .regex(/^[a-zA-Z ]+$/)
          .optional(),
        userName: joi.string().required(),
        email: joi.string().email().required(),
        phoneNo: joi
          .string()
          .regex(/^[0-9]{5,}$/)
          .required(),
        countryCode: joi
          .string()
          .regex(/^[0-9+]{1,}$/)
          .required(),
        password: joi.string().required(),
        profilePic: joi.object().keys({
          original: joi.string().allow("").optional(),
          thumbNail: joi.string().allow("").optional(),
        }),
        addresses: joi
          .array()
          .items(
            joi.object().keys({
              addressType: joi.string().allow("").optional(),
              country: joi.string().allow("").optional(),
              state: joi.string().allow("").optional(),
              city: joi.string().allow("").optional(),
              address: joi.string().allow("").optional(),
              latitude: joi.number().optional(),
              longitude: joi.number().optional(),
              isDefault: joi.boolean().optional(),
            })
          )
          .optional(),
      });
      await validateSchema(req.body, schema);
      next();
    } catch (error) {
      next(error);
    }
}
const validateLogin = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            email: joi.string().email().required(),
            password: joi.string().required()
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
};
const validateLogout = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            isLogoutAllDevice: joi.boolean().optional(),
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateProfile = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
        });
        await validateSchema(req.params, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateUpdateProfile = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            firstName: joi.string().regex(/^[a-zA-Z ]+$/).optional(),
            lastName: joi.string().regex(/^[a-zA-Z ]+$/).optional(),
            profilePic: joi.object().keys({
                original:joi.string().allow("").optional(),
                thumbNail:joi.string().allow("").optional(),
            }),
            addresses:joi.array().items(
                joi.object().keys({
                    addressType:joi.string().allow("").optional(),
                    country:joi.string().allow("").optional(),
                    state:joi.string().allow("").optional(),
                    city:joi.string().allow("").optional(),
                    address:joi.string().allow("").optional(),
                    latitude:joi.number().optional(),
                    longitude:joi.number().optional(),
                    isDefault:joi.boolean().optional()
                }),
            ).optional()
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateChangePassword = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            oldPassword: joi.string().required(),
            newPassword: joi.string().required(),
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateSendOtp = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            phoneNo: joi
                .string()
                .regex(/^[0-9]{5,}$/)
                .required(),
            countryCode: joi
                .string()
                .regex(/^[0-9+]{1,}$/)
                .required(),
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateVerifyOtp = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            otpCode: joi.string().required(),
            phoneNo: joi
                .string()
                .regex(/^[0-9]{5,}$/)
                .required(),
            countryCode: joi
                .string()
                .regex(/^[0-9+]{1,}$/)
                .required(),
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateSendVerifyEmail = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateVerifyEmail = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            verificationEmailToken: joi.string().required()
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateForgotPassword = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            email: joi.string().email().required(),
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateResetPassword = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            passwordResetToken: joi.string().required(),
            newPassword: joi.string().required(),
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateUploadDoc = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            docFile:joi.any().required()
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
const validateUploadProfileImage = async(req:Request, res:Response, next:NextFunction) => {
    try {
        let schema = joi.object().keys({
            image:joi.any().required()
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
}
/**
 *
 * @param {*} params
 * @description for get Users
 * @returns
 */
const validateUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
export {
    validateSignUp,
    validateLogin,
    validateLogout,
    validateProfile,
    validateUpdateProfile,
    validateChangePassword,
    validateSendOtp,
    validateVerifyOtp,
    validateSendVerifyEmail,
    validateVerifyEmail,
    validateForgotPassword,
    validateResetPassword,
    validateUploadDoc,
    validateUploadProfileImage,
    validateUsers
}