export interface ICheckUserExists{
    email?:string;
    phoneNo?:string;
    userName?:string;
    isSignUpRequest?:boolean;
    userId?:string;
}
export interface IVerifyOtp{
    otpCode?:string;
    phoneNo?:string;
    prefix?:string;
}
export interface IVerifyVerificationEmailLink{
    verificationEmailToken?:string;
    prefix?:string;
}
export interface IProfiles{
    pageNo?:number;
    limit?:number;
    search?:string;
}
export interface IReadUser<T> {
    checkUserExists(params:any): Promise<any>;
    userDetails(params:any): Promise<any>;
    profiles(params:IProfiles):Promise<any>;
}
export interface IWriteUser<T> {
    createUser(params:any): Promise<any>;
    updateUserDetails(params:any): Promise<any>;
}
