import SERVER from "../../../../config/environment";
import CONSTANT from "../../../../constant/constant";
import MESSAGES from "../../../../messages/messages";
import { STATUS } from "../../../../enums/enums";
import {
  generateNumber,
  renderMessage,
  randomString,
  readFile,
  isJson,
} from "../../../../lib/universal-function";
import { getData, deleteData } from "../../../../services/redis";
import { genrateAuthToken } from "../../common/services/auth";
import { ISendOtp } from "../../common/interfaces/otp";
import { sendOtp } from "../../common/services/otp";
import { ISendEmailLink } from "../../common/interfaces/email";
import { sendEmailLink } from "../../common/services/email";
import {
  ICheckUserExists,
  IVerifyVerificationEmailLink,
  IVerifyOtp,
  IProfiles
} from "../interfaces/users";
import { userDao } from "../dao/users";

const availabilityCheckEmailPhoneNoUserName = async (param: ICheckUserExists) => {
  let { email,phoneNo,isSignUpRequest,userId,userName } = param;
  let user = await userDao.checkUserExists({userId, email,phoneNo,userName });
  if (!user) return true
  if (user && email && user.email==email && isSignUpRequest) throw new Error(MESSAGES.EMAIL_ALREDAY_EXIT);
  else if (user && phoneNo && user.phoneNo==phoneNo && isSignUpRequest) throw new Error(MESSAGES.PHONE_NUMBER_ALREADY_EXISTS);
  else if (user && userName && user.userName==userName && isSignUpRequest) throw new Error(MESSAGES.USERNAME_ALREADY_EXISTS);
  return user;
};
const checkUserExists = async (param: ICheckUserExists) => {
  let { email,phoneNo,userName,isSignUpRequest } = param;
  let user = await userDao.checkUserExists({ email ,phoneNo,userName});
  if (!user) throw new Error(MESSAGES.USER_NOT_FOUND);
  else if (user && email && isSignUpRequest) throw new Error(MESSAGES.EMAIL_ALREDAY_EXIT);
  else if (user && phoneNo && isSignUpRequest) throw new Error(MESSAGES.PHONE_NUMBER_ALREADY_EXISTS);
  else if (user && userName && isSignUpRequest) throw new Error(MESSAGES.USERNAME_ALREADY_EXISTS);
  if (user.status == STATUS.IN_ACTIVATED) throw new Error(MESSAGES.USER_IS_BLOCKED);
  return user;
};
/**
 * 
 * @param {*} params 
 * @description for create user
 * @returns 
 */
const createUser=async(params:any)=> {
  return await userDao.createUser(params);
}
const genrateUserAuthToken = async (user: TokenData) => {
  const deviceId = user.userId + "-" + new Date().getTime();
  let accessToken = await genrateAuthToken(Models.USER_SESSIONS, {
    ...user,
    deviceId,
  });
  return { deviceId, accessToken };
};
const userSendOtp = async (user: ISendOtp) => {
  let otpCode = await generateNumber();
  let message = await renderMessage(CONSTANT.NOTIFICATIONS.OTP.MESSAGE, {
    otpCode,
  });
  sendOtp({ ...user, message, otpCode });
};
/**
 *
 * @param {otpCode,prefix,phoneNo} params
 * @description for verify OTP
 * @returns
 */
async function userVerifyOtp(params: IVerifyOtp) {
  let { otpCode, prefix, phoneNo } = params;
  if (!prefix) prefix = CONSTANT.PREFIX.OTP;
  let key = prefix + otpCode;
  let otpData = await getData(key);
  if (!otpData) throw new Error(MESSAGES.EXIPRE_OTP_CODE);

  otpData = await isJson(otpData);
  if (!otpData) throw new Error(MESSAGES.EXIPRE_OTP_CODE);
  if (otpData && otpData.phoneNo != phoneNo)
    throw new Error(MESSAGES.INVALID_OTP);
  deleteData(key);
  return otpData;
}
/**
 *
 * @param {email,userId,prefix,subject,content,token} params
 * @description for send verification email link
 * @returns
 */
const userSendEmailLink = async (user: ISendEmailLink) => {
  let verificationEmailToken = await randomString();
  let subject = await renderMessage(
    CONSTANT.NOTIFICATIONS.USER_EMAIL_VARIFICATION_LINK.SUBJECT,
    {}
  );
  let content = await readFile(
    CONSTANT.NOTIFICATIONS.USER_EMAIL_VARIFICATION_LINK.PATH
  );
  let verificationLink =
    SERVER.USER_EMAIL_VERIFICATION_PAGE +
    "?verificationEmailToken=" +
    verificationEmailToken;
    console.log("verificationEmailToken",verificationEmailToken)
  content = await renderMessage(content, { verificationLink });
  sendEmailLink({
    ...user,
    subject,
    content,
    token: verificationEmailToken,
  });
};
/**
 *
 * @param {verificationEmailToken,prefix} params
 * @description for verify verification email link
 * @returns
 */
async function userVerifyVerificationEmailLink(
  params: IVerifyVerificationEmailLink
) {
  let { verificationEmailToken, prefix } = params;
  if (!prefix) prefix = CONSTANT.PREFIX.EMAIL_VARIFICATION_LINK;
  let key = prefix + verificationEmailToken;
  let emailData = await getData(key);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);
  emailData = await isJson(emailData);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);
  deleteData(key);
  return emailData;
}
/**
 *
 * @param {*} params
 * @description for profiles
 * @returns
 */
const profiles = async (params: IProfiles) => {
  return await userDao.profiles(params);
};
export {
  availabilityCheckEmailPhoneNoUserName,
  checkUserExists,
  createUser,
  genrateUserAuthToken,
  userSendOtp,
  userVerifyOtp,
  userSendEmailLink,
  userVerifyVerificationEmailLink,
  profiles
};
