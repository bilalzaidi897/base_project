import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import CONSTANT from "../../../../constant/constant";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse, isJson } from "../../../../lib/universal-function";
import {
  availabilityCheckEmailPhoneNoUserName,
  checkUserExists,
  createUser,
  genrateUserAuthToken,
  userSendOtp,
  userVerifyOtp,
  userSendEmailLink,
  userVerifyVerificationEmailLink,
  profiles
} from "../services/users";
import { deleteSessions } from "../../common/services/session";
import { userDao } from "../dao/users";
import { STATUS } from "../../../../enums/enums";
import {
  uploadImageToS3Bucket,
  uploadOriginalImageToS3Bucket,
} from "../../../../services/s3";
import { getData, deleteData } from "../../../../services/redis";
import { IProfiles } from "../interfaces/users";
/**
 *
 * @param {*} params
 * @description for singUp
 * @returns
 */
const singUp = asyncHandler(async (req: Request, res: Response) => {
  let { devicetype: deviceType, deviceName: deviceName } = req.headers;
  let ip = req.ip;
  let params = req.body;
  let { email, phoneNo, password, userName } = params;
  await availabilityCheckEmailPhoneNoUserName({ email,phoneNo,userName, isSignUpRequest: true });
  params.status = STATUS.PENDING;
  const doc = await createUser(params);
  await doc.setPassword(password);
  await doc.save();
  let user = doc.toJSON();
  let tokenData = await genrateUserAuthToken({
    ...user,
    ip,
    deviceType,
    deviceName,
  });
  user = { ...user, ...tokenData };
  // if (!user.isPhoneNoVerified) {
  //   userSendOtp({...user});
  // }
  // if (!user.isEmailVerified) {
  //   userSendEmailLink(user);
  // }
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SUCCESSFULLY_REGISTERED,
    user
  );
});
/**
 *
 * @param {*} params
 * @description for login
 * @returns
 */
const login = asyncHandler(async (req: Request, res: Response) => {
  let { devicetype: deviceType, deviceName: deviceName } = req.headers;
  let ip = req.ip;
  let { email, password } = req.body;
  let user = await checkUserExists({ email });
  await user.authenticate(password);
  user = user.toJSON();
  let tokenData = await genrateUserAuthToken({
    ...user,
    ip,
    deviceType,
    deviceName,
  });
  user = { ...user, ...tokenData };
  // if (!user.isPhoneNoVerified) {
  //   userSendOtp({...user});
  // }
  // if (!user.isEmailVerified) {
  //   userSendEmailLink(user);
  // }
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, user);
});
const logout = asyncHandler(async (req: any, res: Response) => {
  let { deviceId, userId } = req.user;
  await deleteSessions(Models.USER_SESSIONS, {
    deviceId,
    userId,
    ...req.body,
  });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.LOGOUT_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for profile
 * @returns
 */
const profile = asyncHandler(async (req: Request, res: Response) => {
  let { userId }: any = req.user;
  let user = await userDao.userDetails({ userId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, user);
});
/**
 *
 * @param {*} params
 * @description for update profile
 * @returns
 */
const updateProfile = asyncHandler(async (req: Request, res: Response) => {
  let params = req.body;
  let { userId } = params;
  let user = await userDao.updateUserDetails({
    ...params,
    userId,
  });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.PROFILE_UPDATED, user);
});
/**
 *
 * @param {*} params
 * @description for change password
 * @returns
 */
const changePassword = asyncHandler(async (req: Request, res: Response) => {
  let { oldPassword, newPassword } = req.body;
  let { userId }: any = req.user;
  const user = await userDao.userDetails({ userId });
  await user.authenticate(oldPassword);
  await user.setPassword(newPassword);
  await user.save();
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for send OTP
 * @returns
 */
const sendOtp = asyncHandler(async (req: Request, res: Response) => {
  let { phoneNo, countryCode }: any = req.body;
  let { userId }: any = req.user;
  await availabilityCheckEmailPhoneNoUserName({
    userId,
    phoneNo,
    isSignUpRequest: true,
  });
  let user = await userDao.userDetails({ userId });
  user = user.toJSON();
  if (!user) throw new Error(MESSAGES.USER_NOT_FOUND);
  userSendOtp({ userId, phoneNo: phoneNo, countryCode: countryCode });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SEND_OTP_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for verify OTP
 * @returns
 */
const verifyOtp = asyncHandler(async (req: Request, res: Response) => {
  let { otpCode, phoneNo, countryCode } = req.body;
  let user = await userVerifyOtp({ otpCode, phoneNo });
  let update: any = {
    isPhoneNoVerified: true,
    phoneNo,
    countryCode,
  };
  if (user.isEmailVerified) update.status = STATUS.ACTIVE;
  await availabilityCheckEmailPhoneNoUserName({
    userId: user?.userId,
    phoneNo,
    isSignUpRequest: true,
  });
  await userDao.updateUserDetails({ ...update, userId: user?.userId });
  if (user.isEmailVerified)
    await deleteSessions(Models.USER_SESSIONS, {
      userId: user?.userId,
      isLogoutAllDevice: true,
    });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.OTP_VERIFIED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for send verify email
 * @returns
 */
const sendVerifyEmail = asyncHandler(async (req: Request, res: Response) => {
  let { userId }: any = req.user;
  let user = await userDao.userDetails({ userId });
  user = user.toJSON();
  if (!user) throw new Error(MESSAGES.USER_NOT_FOUND);
  if (user.status == STATUS.IN_ACTIVATED)
    throw new Error(MESSAGES.USER_IS_BLOCKED);
  if (!user.isEmailVerified && user?.email) {
    userSendEmailLink({ userId: userId, email: user?.email });
  }
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SEND_EMAIL_VERIFICATION_LINK,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for verify email
 * @returns
 */
const verifyEmail = asyncHandler(async (req: Request, res: Response) => {
  let { verificationEmailToken } = req.body;
  let emailVerifyData = await userVerifyVerificationEmailLink({
    verificationEmailToken,
  });
  if (!emailVerifyData || (emailVerifyData && !emailVerifyData.userId))
    throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);

  let user = await userDao.userDetails({
    userId: emailVerifyData.userId,
  });
  user = user.toJSON();
  if (!user) throw new Error(MESSAGES.USER_NOT_FOUND);
  let update: any = {
    isEmailVerified: true,
  };
  if (user.isPhoneNoVerified) {
    update.status = STATUS.ACTIVE;
    await deleteSessions(Models.USER_SESSIONS, {
      userId: user.userId,
      isLogoutAllDevice: true,
    });
  }
  await userDao.updateUserDetails({
    ...update,
    userId: user.userId,
  });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.EMAIL_VERIFIED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for forgot password
 * @returns
 */
const forgotPassword = asyncHandler(async (req: Request, res: Response) => {
  let { email } = req.body;
  let prefix = CONSTANT.PREFIX.FORGOT_PASSWORD;
  let user = await checkUserExists({ email });
  user = user.toJSON();
  if (!user) throw new Error(MESSAGES.USER_NOT_FOUND);
  userSendEmailLink({ ...user, prefix });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.FORGOT_PASSWORD_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for reset password
 * @returns
 */
const resetPassword = asyncHandler(async (req: Request, res: Response) => {
  let { passwordResetToken, newPassword } = req.body;
  let prefix = CONSTANT.PREFIX.FORGOT_PASSWORD;
  let key = prefix + passwordResetToken;
  let emailData = await getData(key);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_RESET_PASSWORD_LINK);

  emailData = await isJson(emailData);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_RESET_PASSWORD_LINK);
  deleteData(key);
  let { userId } = emailData;
  const user = await userDao.userDetails({ userId });
  await user.setPassword(newPassword);
  await user.save();
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for upload Doc
 * @returns
 */
const uploadDoc = asyncHandler(async (req: any, res: Response) => {
  let s3File: any = {};
  if (req.files && req.files.docFile && req.files.docFile.size > 0) {
    s3File = await uploadOriginalImageToS3Bucket(
      req.files.docFile,
      CONSTANT.S3_FOLDERS.USERS
    );
  }
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, s3File);
});
/**
 *
 * @param {*} params
 * @description for upload profile image
 * @returns
 */
const uploadProfileImage = asyncHandler(async (req: any, res: Response) => {
  let data: any = {};
  if (
    req.files &&
    req.files.image &&
    req.files.image.size &&
    req.files.image.size < 3072000
  ) {
    data = await uploadImageToS3Bucket(
      req.files.image,
      CONSTANT.S3_FOLDERS.USERS
    );
  }
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, data);
});
/**
 *
 * @param {*} params
 * @description for profiles
 * @returns
 */
const getProfiles = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query as unknown as IProfiles;
  let profilesData = await profiles({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    profiles: profilesData?.profiles || [],
    count: profilesData?.count || 0,
  });
});
export {
  singUp,
  login,
  logout,
  profile,
  updateProfile,
  changePassword,
  sendOtp,
  verifyOtp,
  sendVerifyEmail,
  verifyEmail,
  forgotPassword,
  resetPassword,
  uploadDoc,
  uploadProfileImage,
  getProfiles
};
