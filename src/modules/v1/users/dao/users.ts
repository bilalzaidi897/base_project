import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  IWriteUser,
  IReadUser,
  ICheckUserExists,
  IProfiles
} from "../interfaces/users";
export class UserDao<T>
  extends BaseDao
  implements IReadUser<T>, IWriteUser<T>
{
  async checkUserExists(
    params: ICheckUserExists
  ): Promise<any> {
    let { email, phoneNo, userId ,userName} = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    query["$or"]=[];
    if (email) query["$or"].push({email:email});
    if (phoneNo) query["$or"].push({phoneNo:phoneNo});
    if (userName) query["$or"].push({userName:userName});
    if (userId) query._id = { $ne: userId };
    return await this.findOne(Models.USERS, query, {}, {}, {});
  }
  async userDetails(params: any): Promise<any> {
    let { userId } = params;
    let query = {
      _id: userId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.USERS, query, {}, {}, {});
  }
  async createUser(param:any): Promise<any> {
    return this.save(Models.USERS,param);
  }
  async updateUserDetails(params: any): Promise<any> {
    let { userId } = params;
    let query = {
      _id: userId,
      status: { $ne: STATUS.DELETED },
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.USERS, query, update, {});
  }
  async profiles(params: IProfiles): Promise<any> {
    let { search, limit = 0, pageNo } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (search) query["$or"] = [{ firstName: search },{email:search}, { phoneNo: search }];

    let profilesData = await this.find(
      Models.USERS,
      query,
      {},
      {},
      {},
      { pageNo, limit },
      {}
    );
    let count = await this.count(Models.USERS, query);
    return { profiles: profilesData, count };
  }
}
export const userDao = new UserDao();
