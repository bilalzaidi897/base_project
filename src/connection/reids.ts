import { createClient } from "redis";
import SERVER from "../config/environment";

let redisClient: any = createClient({
  url: SERVER.REDIS.DB_URL,
});
const connectRedis = async () => {
  try {
    await redisClient.connect();
    redisClient.Promise = global.Promise; // Get Redis to use the global promise library
    console.log("Redis client connected...");
    return redisClient;
  } catch (err) {
    console.log(err);
    setTimeout(connectRedis, 5000);
  }
};
redisClient.on("error", (err: any) => console.log(err));
export default { redisClient, connectRedis };
