import mongoose from "mongoose";
import SERVER from "../config/environment";

const connectMongoDB = async () => {
  try {
    mongoose.set('strictQuery', false);
    await mongoose.connect(SERVER.MONGO.DB_URL || "", SERVER.MONGO.OPTIONS);
    mongoose.Promise = global.Promise; // Get Mongoose to use the global promise library
    const db: mongoose.Connection = mongoose.connection; // Get the default connection
    console.log("Mongo database connected...");
    return db;
  } catch (error) {
    console.log(error);
    setTimeout(connectMongoDB, 5000);
  }
};

export default connectMongoDB;
