import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS, PAYMENT_MODE } from "../enums/enums";
export interface ITransactions {
  _id: string;
  userId: ObjectId;
  transactionId: ObjectId;
  amount: Number;
  paidAmount: Number;
  card: any;
  paymentMode:
     PAYMENT_MODE.APPLE_PAY
    | PAYMENT_MODE.CARD
    | PAYMENT_MODE.CASH
    | PAYMENT_MODE.NO_PAYMENT;
  status:STATUS.PENDING | STATUS.REFUND | STATUS.COMPLETED | STATUS.DELETED;
}
type TransactionType = ITransactions & mongoose.Document;
const transactionSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    userId: { type: Schema.Types.ObjectId, ref: "users" },
    transactionId: { type: String, default: "" },
    amount: { type: Number, default: 0 },
    paidAmount: { type: Number, default: 0 },
    card: { type: Object, default: null },
    paymentMode: {
      type: Number,
      enum: [
        PAYMENT_MODE.APPLE_PAY,
        PAYMENT_MODE.CARD,
        PAYMENT_MODE.CASH,
        PAYMENT_MODE.NO_PAYMENT,
      ],
      default: PAYMENT_MODE.NO_PAYMENT,
    },
    status: {
      type: Number,
      enum: [STATUS.REFUND, STATUS.COMPLETED, STATUS.PENDING,STATUS.DELETED],
      default: STATUS.PENDING,
    },
  },
  {
    timestamps: true,
  }
);

const Transactions: Model<TransactionType> = mongoose.model<TransactionType>(
  "transactions",
  transactionSchema
);
export default Transactions;
