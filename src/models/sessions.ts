import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { DEVICE_TYPE,STATUS } from "../enums/enums";
export interface ISessions {
  _id: string;
  userId?: ObjectId;
  deviceType:
     DEVICE_TYPE.DEFAULT
    | DEVICE_TYPE.ANDROID
    | DEVICE_TYPE.IOS
    | DEVICE_TYPE.WEB;
  deviceId?: string;
  deviceToken?: string;
  deviceName: string;
  ip: string;
  status:
   STATUS.DEFAULT
  | STATUS.DELETED;
}
type SessionType = ISessions & mongoose.Document;
const sessionSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    userId: { type: Schema.Types.ObjectId, ref: "users", index: true },
    deviceType: {
      type: Number,
      enum: [
        DEVICE_TYPE.DEFAULT,
        DEVICE_TYPE.ANDROID,
        DEVICE_TYPE.IOS,
        DEVICE_TYPE.WEB,
      ],
      default: DEVICE_TYPE.DEFAULT,
    },
    deviceId: { type: String, index: true },
    accessToken: { type: String },
    deviceName: { type: String, default: "" },
    ip: { type: String, default: "" },
    status: {
      type: Number,
      enum: [
        STATUS.DEFAULT,
        STATUS.DELETED
      ],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
const Sessions: Model<SessionType> = mongoose.model<SessionType>(
  "sessions",
  sessionSchema
);
export default Sessions;
