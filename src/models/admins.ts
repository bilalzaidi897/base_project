import * as mongoose from "mongoose";
import { Model, Schema ,ObjectId} from "mongoose";
import bcrypt from "bcryptjs";
import { ADMIN_TYPE, STATUS } from "../enums/enums";
import messages from "../messages/messages";

export interface IAdmin {
  _id: string;
  roleId?: ObjectId[];
  parentId?:ObjectId;
  firstName?: string;
  lastName?: string;
  email: string;
  phoneNo: string;
  countryCode: string;
  password: string;
  profilePic: any;
  addresses: any;
  adminType: ADMIN_TYPE.ADMIN | ADMIN_TYPE.ADMIN_USER | ADMIN_TYPE.COMPANY | ADMIN_TYPE.COMPANY_USER;
  status:
    STATUS.DEFAULT
    | STATUS.PENDING
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED 
    | STATUS.DELETED;
}

type AdminType = IAdmin & mongoose.Document;
const adminSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    roleId: [{
      type: Schema.Types.ObjectId,
      ref: "roles",
      index: true,
      default: null,
    }],
    parentId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    firstName: { type: String, default: "" },
    lastName: { type: String, default: "" },
    email: { type: String, index: true, default: "" },
    phoneNo: { type: String, index: true, default: "" },
    countryCode: { type: String, index: true, default: "" },
    password: { type: String, index: true, default: "" },
    profilePic: {
      original: { type: String, default: "" },
      thumbNail: { type: String, default: "" },
    },
    addresses: [
      {
        addressType: { type: String, default: "" },
        country: { type: String, default: "" },
        state: { type: String, default: "" },
        city: { type: String, default: "" },
        address: { type: String, default: "" },
        latitude: { type: Number, default: 0 },
        longitude: { type: Number, default: 0 },
        isDefault: { type: Boolean, default: true },
      },
    ],
    adminType: {
      type: Number,
      enum: [ADMIN_TYPE.ADMIN,ADMIN_TYPE.ADMIN_USER,
        ADMIN_TYPE.COMPANY,
        ADMIN_TYPE.COMPANY_USER],
      default: ADMIN_TYPE.ADMIN_USER,
    },
    status: {
      type: Number,
      enum: [
        STATUS.DEFAULT,
        STATUS.PENDING,
        STATUS.ACTIVE,
        STATUS.IN_ACTIVATED,
        STATUS.DELETED
      ],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);

adminSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.adminId=(doc._id).toString()
    delete ret._id;
    delete ret.password;
    delete ret.__v;
  },
});

adminSchema.methods.authenticate = function (password: string, cb: any) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error(messages.MISSING_PASSWORD));
    bcrypt.compare(password, this.password, (error, result) => {
      if (!result) reject(new Error(messages.WRONG_PASSWORD));
      resolve(this);
    });
  });

  if (!cb) return promise;
  promise.then((result) => cb(null, result)).catch((err) => cb(err));
};

adminSchema.methods.setPassword = function (password: string, cb: any) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error(messages.MISSING_PASSWORD));

    bcrypt.hash(password, 10, (err, hash) => {
      if (err) reject(err);
      this.password = hash;
      resolve(this);
    });
  });

  if (!cb) return promise;
  promise.then((result) => cb(null, result)).catch((err) => cb(err));
};
const Admins: Model<AdminType> = mongoose.model<AdminType>(
  "admins",
  adminSchema
);
export default Admins;
 