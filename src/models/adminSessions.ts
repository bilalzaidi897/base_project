import * as mongoose from "mongoose";
import { Model, Schema ,ObjectId} from "mongoose";
import { DEVICE_TYPE ,STATUS} from "../enums/enums";
export interface IAdminSessions {
  _id: string;
  adminId?: ObjectId;
  deviceType:
     DEVICE_TYPE.DEFAULT
    | DEVICE_TYPE.ANDROID
    | DEVICE_TYPE.IOS
    | DEVICE_TYPE.WEB;
  deviceId?: string;
  deviceToken?: string;
  deviceName: string;
  ip: string;
  status:
   STATUS.DEFAULT
  | STATUS.DELETED;
}
type AdminSessionType = IAdminSessions & mongoose.Document;
const adminSessionSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    adminId: { type: Schema.Types.ObjectId, ref: "admins", index: true },
    deviceType: {
      type: Number,
      enum: [
        DEVICE_TYPE.DEFAULT,
        DEVICE_TYPE.ANDROID,
        DEVICE_TYPE.IOS,
        DEVICE_TYPE.WEB,
      ],
      default: DEVICE_TYPE.DEFAULT,
    },
    deviceId: { type: String, index: true },
    accessToken: { type: String },
    deviceName: { type: String, default: "" },
    ip: { type: String, default: "" },
    status: {
      type: Number,
      enum: [
        STATUS.DEFAULT,
        STATUS.DELETED
      ],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
const AdminSessions: Model<AdminSessionType> = mongoose.model<AdminSessionType>(
  "adminSessions",
  adminSessionSchema
);
export default AdminSessions;
