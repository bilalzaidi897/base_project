import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS } from "../enums/enums";

export interface IModule {
  moduleId: ObjectId;
  add: Boolean;
  edit: Boolean;
  view: Boolean;
  delete: Boolean;
}
export interface IRoles {
  _id: string;
  role: string;
  modules: IModule[];
  status:
     STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
}
type RoleType = IRoles & mongoose.Document;
const roleSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    role: { type: String, default: "" },
    description: { type: String, default: "" },
    modules: [
      {
        moduleId: {
          type: Schema.Types.ObjectId,
          ref: "modules",
          default: null,
        },
        add: { type: Boolean, default: false },
        edit: { type: Boolean, default: false },
        view: { type: Boolean, default: false },
        delete: { type: Boolean, default: false },
      },
    ],
    status: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.ACTIVE,STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
roleSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.roleId = doc._id.toString();
    delete ret.__v;
  },
});

const Roles: Model<RoleType> = mongoose.model<RoleType>(
  "roles",
  roleSchema
);
export default Roles;

