import * as mongoose from "mongoose";
import { Model, Schema } from "mongoose";
import { STATUS } from "../enums/enums";

export interface IModules {
  _id: string;
  name: string;
  add: Boolean;
  edit: Boolean;
  view: Boolean;
  delete: Boolean;
  status:
     STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
}
type ModuleType = IModules & mongoose.Document;
const moduleSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    name: { type: String, default: "" },
    add: { type: Boolean, default: false },
    edit: { type: Boolean, default: false },
    view: { type: Boolean, default: false },
    delete: { type: Boolean, default: false },
    status: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.ACTIVE,STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
moduleSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.moduleId = doc._id.toString();
    delete ret.__v;
  },
});

const Modules: Model<ModuleType> = mongoose.model<ModuleType>(
  "modules",
  moduleSchema
);
export default Modules;
