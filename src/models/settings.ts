import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS } from "../enums/enums";

export interface ISetting {
  _id: string;
  status:
    STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED
}
type SettingType = ISetting & mongoose.Document;
const settingSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    status: {
      type: Number,
      enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED,STATUS.DELETED],
      default: STATUS.ACTIVE,
    },
  },
  {
    timestamps: true,
  }
);

const Settings: Model<SettingType> = mongoose.model<SettingType>(
  "settings",
  settingSchema
);
export default Settings;
