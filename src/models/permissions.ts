import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS } from "../enums/enums";
export interface IPermissions {
  _id: string;
  moduleId: ObjectId;
  addApis?: string[];
  editApis?: string[];
  viewApis?: string[];
  deleteApis?: string[];
  status:
    STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
};
type PermissionType = IPermissions & mongoose.Document;
const permissionSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    moduleId: { type: Schema.Types.ObjectId, ref: "modules", default: null },
    addApis: [{ type: String, default: "" }],
    editApis: [{ type: String, default: "" }],
    viewApis: [{ type: String, default: "" }],
    deleteApis: [{ type: String, default: "" }],
    status: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.ACTIVE,STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
permissionSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.permissionId = doc._id.toString();
    delete ret.__v;
  },
});

const Permissions: Model<PermissionType> = mongoose.model<PermissionType>(
  "permissions",
  permissionSchema
);
export default Permissions;
