import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { CATALOGUE_TYPE, STATUS } from "../enums/enums";
import {elasticSearch} from "../modules/v1/common/dao/elasticSearch";
import CONSTANT from "../constant/constant"
import {setData} from "../services/redis"
export interface ICatalogues {
  _id: string;
  parentId?: ObjectId;
  categoryId?: ObjectId;
  serviceId?: ObjectId;
  combo?: ObjectId[];
  name: string;
  description?: string;
  tags?: string[];
  searchTags?: string[];
  sku?: string;
  price?: number;
  profilePic?:{
    original:string;
    thumbNail:string;
  };
  translations?: Object;
  catalogueType:
    | CATALOGUE_TYPE.CATEGORY
    | CATALOGUE_TYPE.SERVICE
    | CATALOGUE_TYPE.PRODUCT;
  status: STATUS.DEFAULT | STATUS.DELETED;
}
type CataloguesType = ICatalogues & mongoose.Document;
const catalogueSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    adminId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    parentId: {
      type: Schema.Types.ObjectId,
      ref: "catalogues",
      index: true,
      default: null,
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: "catalogues",
      index: true,
      default: null,
    },
    serviceId: {
      type: Schema.Types.ObjectId,
      ref: "catalogues",
      index: true,
      default: null,
    },
    combo: [
      {
        type: Schema.Types.ObjectId,
        ref: "catalogues",
        index: true,
        default: null,
      },
    ],
    name: { type: String, index: true },
    description: { type: String, default: "" },
    tags: [{ type: String, default: "" }],
    searchTags: [{ type: String, default: "" }],
    sku: { type: String, default: "", index: true },
    price: { type: Number, default: 0 },
    profilePic: {
      original: { type: String, default: "" },
      thumbNail: { type: String, default: "" },
    },
    translations: { type: Object, default: {} },
    catalogueType: {
      type: Number,
      enum: [
        CATALOGUE_TYPE.CATEGORY,
        CATALOGUE_TYPE.SERVICE,
        CATALOGUE_TYPE.PRODUCT,
      ],
      default: CATALOGUE_TYPE.PRODUCT,
    },
    status: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.DELETED],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
// catalogueSchema.post('save', doc => console.log('[create] says:', doc));

// catalogueSchema.post('updateOne', doc => console.log('[update] says:', doc));
// catalogueSchema.post('update', doc => console.log('[update] says:', doc));
// catalogueSchema.post('updateMany', doc => console.log('[update] says:', doc));
// catalogueSchema.post('findOneAndUpdate', doc => console.log('[findOneAndUpdate] says:', doc));

// catalogueSchema.post('deleteOne', doc => console.log('[deleteOne] says:', doc));
// catalogueSchema.post('deleteMany', docs => console.log('[partial:x] says:', docs));
// catalogueSchema.post('findOneAndDelete', doc => console.log('[findOneAndDelete] says:', doc));
// catalogueSchema.post('findOneAndRemove', doc => console.log('[findOneAndRemove] says:', doc));
// catalogueSchema.post('remove', doc => console.log('[remove] says:', doc));
// catalogueSchema.post('findOneAndReplace', doc => console.log('[findOneAndReplace] says:', doc));
// catalogueSchema.post('replaceOne', doc => console.log('[replaceOne] says:', doc));

const Catalogues: Model<CataloguesType> = mongoose.model<CataloguesType>(
  "catalogues",
  catalogueSchema
);

Catalogues.watch([], { fullDocument: "updateLookup" }).
  on('change', (docs:any) => {
    console.log(docs?.fullDocument)
    if(docs?.fullDocument){
      let doc=docs?.fullDocument;
      doc.id = String(doc._id);
      delete doc._id;
      const catalogueIds = [
        doc.parentId,
        doc.categoryId,
        doc.serviceId,
        ...doc.combo].filter(id => {
        return id !== null && id !== undefined;
      });
      doc["suggestSearchTags"] = {
        input: [doc.name,doc.description,...doc.searchTags,doc.sku],
        contexts: {
          catalogueId:catalogueIds,
          catalogueType:[doc.catalogueType],
          status: [doc.status]
        },
      };
      setData(CONSTANT.ELASTIC_SEARCH_INDEXES.CATALOGUES,(new Date().getTime()).toString())
      elasticSearch.addDocument((CONSTANT.ELASTIC_SEARCH_INDEXES.CATALOGUES),doc.id,doc)
    }
  });
export default Catalogues;
