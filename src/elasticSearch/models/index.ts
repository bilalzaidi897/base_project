export default {
    "catalogues": {
        properties: {
          "id": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "parentId": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "categoryId": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "serviceId": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "combo": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "name": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "description": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 500
              }
            }
          },
          "tags": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "searchTags": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "suggestSearchTags": {
            "type": "completion",
            "contexts": [
              {
                "name": "catalogueId",
                "type": "category",
                "path": "catalogueId"
              },
              {
                "name": "catalogueType",
                "type": "category",
                "path": "catalogueType"
              },
              {
                "name": "status",
                "type": "category",
                "path": "status"
              }
            ]
          },
          "sku": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "price": {
            "type": "long"
          },
          "translations": {
            "type": "object",
            "dynamic": true,
            "properties": {
              
            }
          },
          "catalogueType": {
            "type": "long"
          },
          "status": {
            "type": "long"
          },
          "updatedAt": {
            "type": "date"
          },
          "createdAt": {
            "type": "date"
          }
        }
    }
}