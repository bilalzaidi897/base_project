const PROJECT_NAME = "Mentro-matching";

const SERVICE_BASE_URL = "service";
const BATCH_SIZE_OF_NOTIFICAION = 500;
const DEFAULT_LIMIT = 10;
const DEFAULT_SKIP = 0;

const COUNTER = {
  SESSION: 10000,
  OTP: 3,
  EMAIL_VARIFICATION_LINK: 3,
}

const LANGUAGE_TYPE = {
  DEFAULT: "en",
  ENGLISH: "en",
  ARABIC: "ar",
}

const LOGGER = {
  LOGGER_ON: true,
  CONSOLE_ON: true,
  ADD_ERROR_FILE: false,
  ADD_INFO_FILE: false,
  DAILY_ROTATE_LOG_ON: false,
}

const PREFIX = {
  OTP: PROJECT_NAME + "-" + process.env.NODE_ENV + "-otp-",
  EMAIL_VARIFICATION_LINK:PROJECT_NAME +"-" +process.env.NODE_ENV +"-email-verification-link-",
  SOCKET:PROJECT_NAME +"-" +process.env.NODE_ENV +"-socket-",
  FORGOT_PASSWORD:PROJECT_NAME +"-" +process.env.NODE_ENV +"-forgot-password-",
}

const EXPIRE_TIME = {
  OTP_EXPIRE_TIME: 1 * 1 * 2 * 60, // 2 minutes
  OTP_COUNTER_EXPIRE_TIME: 1 * 24 * 60 * 60, // 1 days
  EMAIL_LINK_EXPIRE_TIME: 1 * 1 * 2 * 60, // 2 minutes
  EMAIL_COUNTER_LINK_EXPIRE_TIME: 1 * 24 * 60 * 60, // 1 days
  SOCKET_EXPIRATION_TIME:1 * 24 * 60 * 60, // 1 days
}

const REQUEST_METHOD_TYPE = {
  GET: "get",
  POST: "post",
  FORM_POST: "form_post",
  PUT: "put",
  DELETE: "delete",
}

const SMS_TYPE = {
  DEFAULT: 0,
  TEXT: 1,
  VOICE: 2,
}

const SERVICE_TYPE = {
  DEFAULT: 0,
  TWILIO: 1,
  SNS: 2,
  SES: 3,
  SMTP: 4,
}

const NOTIFICATIONS = {
  ADMIN_EMAIL_VARIFICATION_LINK: {
    PATH: `email-verification.html`,
    SUBJECT: `Reset password link`,
  },
  ADMIN_FORGOT_PASSWORD: {
    PATH: `forgot-password.html`,
    SUBJECT: `Reset password link`,
  },
  OTP: {
    MESSAGE: `Your OTP is:{{otpCode}}`,
  },
  USER_EMAIL_VARIFICATION_LINK: {
    PATH: `email-verification.html`,
    SUBJECT: `Reset password link`,
  },
  USER_FORGOT_PASSWORD: {
    PATH: `forgot-password.html`,
    SUBJECT: `Reset password link`,
  },
  EMAIL_NOTIFICATION_TO_ALL_INVITEES: {
    PATH: `email-invitation.html`,
    SUBJECT: `YouAreTimed Event Invitation`,
  },
  NOTIFICATION_TO_OWNER_BEFORE_EVENT_START_CRON: {
    TITLE: ``,
    MESSAGE: ``,
  },
  NOTIFICATION_TO_OWNER_EVENT_START_CRON: {
    TITLE: ``,
    MESSAGE: ``,
  },
  NOTIFICATION_TO_OWNER_EVENT_CANCEL_CRON: {
    TITLE: ``,
    MESSAGE: ``,
  },
  NOTIFICATION_TO_INVITED_USERS_BEFORE_EVENT_START_CRON: {
    TITLE: ``,
    MESSAGE: ``,
  },
  NOTIFICATION_TO_INVITED_USERS_EVENT_START_CRON: {
    TITLE: ``,
    MESSAGE: ``,
  },
  NOTIFICATION_TO_INVITED_USERS_EVENT_CANCEL_CRON: {
    TITLE: ``,
    MESSAGE: ``,
  },
}

const S3_FOLDERS = {
  USERS: "users",
  ADMINS: "admins",
}

const URLS = ["/sendOtp", "/verifyOtp", "/sendVerifyEmail", "/verifyEmail"];
const REGEX = {
	EMAIL: /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$/,
	// EMAIL: /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	/* URL: /^(http?|ftp|https):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|\
		int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/, */
	URL: /^(https?|http|ftp|torrent|image|irc):\/\/(-\.)?([^\s\/?\.#-]+\.?)+(\/[^\s]*)?$/i,
	SSN: /^(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$/, // US SSN
	ZIP_CODE: /^[0-9]{5}(?:-[0-9]{4})?$/,
	PASSWORD: /(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}/, // Minimum 6 characters, At least 1 lowercase alphabetical character, At least 1 uppercase alphabetical character, At least 1 numeric character, At least one special character
	COUNTRY_CODE: /^\d{1,4}$/,
	MOBILE_NUMBER: /^\d{6,16}$/,
	STRING_REPLACE: /[-+ ()*_$#@!{}|\/^%`~=?,.<>:;'"]/g,
	MONGO_ID: /^[a-f\d]{24}$/i
};
const ELASTIC_SEARCH_INDEXES={
  CATALOGUES: (PROJECT_NAME + "_" + process.env.NODE_ENV +"_catalogues").toLowerCase()
}
export default {
  COUNTER,
  BATCH_SIZE_OF_NOTIFICAION,
  LANGUAGE_TYPE,
  DEFAULT_LIMIT,
  DEFAULT_SKIP,
  LOGGER,
  PREFIX,
  PROJECT_NAME,
  EXPIRE_TIME,
  SERVICE_BASE_URL,
  REQUEST_METHOD_TYPE,
  SMS_TYPE,
  SERVICE_TYPE,
  NOTIFICATIONS,
  S3_FOLDERS,
  URLS,
  REGEX,
  ELASTIC_SEARCH_INDEXES
}
