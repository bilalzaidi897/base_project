import * as dotenv from "dotenv";
import * as fs from "fs";
import * as path from "path";
const { NODE_ENV } = process.env;

switch (NODE_ENV) {
  case "dev":
  case "development": {
    if (fs.existsSync(path.join(process.cwd(), "/.env.development"))) {
      dotenv.config({ path: ".env.development" });
    } else {
      console.log("Unable to find Environment File");
      process.exit(1);
    }
    break;
  }
  case "qa": {
    if (fs.existsSync(path.join(process.cwd(), "/.env.qa"))) {
      dotenv.config({ path: ".env.qa" });
    } else {
      console.log("Unable to find Environment File");
      process.exit(1);
    }
    break;
  }
  case "stag":
  case "staging": {
    if (fs.existsSync(path.join(process.cwd(), "/.env.staging"))) {
      dotenv.config({ path: ".env.staging" });
    } else {
      process.exit(1);
    }
    break;
  }
  case "prod":
  case "production": {
    if (fs.existsSync(path.join(process.cwd(), "/.env"))) {
      dotenv.config({ path: ".env" });
    } else {
      process.exit(1);
    }
    break;
  }
  default: {
    if (fs.existsSync(path.join(process.cwd(), "/.env.local"))) {
      dotenv.config({ path: ".env.local" });
    } else {
      process.exit(1);
    }
  }
}
const SERVER = {
  APP_NAME: "Fleet",
  PORT: process.env["PORT"],
  API_KEY: process.env["API_KEY"],
  FROM_EMAIL: process.env["FROM_EMAIL"],
  FCM_SERVER_KEY: process.env["FCM_SERVER_KEY"],
  ADMIN_EMAIL_VERIFICATION_PAGE: process.env["ADMIN_EMAIL_VERIFICATION_PAGE"],
  USER_EMAIL_VERIFICATION_PAGE: process.env["USER_EMAIL_VERIFICATION_PAGE"],
  APP_LINK: process.env["APP_LINK"],
  JWT: {
    SECRET: process.env["JWT_SECRET"],
    EXPIRESIN: process.env["JWT_EXPIRESIN"]
  },
  REDIS: {
    HOST: process.env["REDIS_HOST"],
    PORT: process.env["REDIS_PORT"],
    DB: process.env["REDIS_DB"],
    USER_NAME: process.env["REDIS_USER_NAME"],
    PASSWORD: process.env["REDIS_PASSWORD"],
    DB_URL: process.env["REDIS_URL"],
  },
  MONGO: {
    DB_NAME: process.env["DB_NAME"],
    DB_URL: process.env["DB_URL"],
    OPTIONS: {
      user: process.env["DB_USER"],
      pass: process.env["DB_PASSWORD"],
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  S3: {
    ACCESS_KEY_ID: process.env["S3_ACCESS_KEY_ID"],
    SECRET_ACCESS_KEY: process.env["S3_SECRET_ACCESS_KEY"],
    BUCKET: process.env["S3_BUCKET"],
    URL: process.env["S3_URL"],
  },
  SNS: {
    REGION: process.env["SNS_REGION"],
    ACCESS_KEY_ID: process.env["SNS_ACCESS_KEY_ID"],
    SECRET_ACCESS_KEY: process.env["SNS_SECRET_ACCESS_KEY"],
    API_VERSION: process.env["SNS_API_VERSION"],
  },
  TWILIO: {
    ACCOUNT_SID: process.env["TWILLO_ACCOUNT_SID"],
    AUTH_TOKEN: process.env["TWILLO_AUTH_TOKEN"],
    TWILIO_NUMBER: process.env["TWILIO_NUMBER"],
  },
  SES: {
    ACCESS_KEY_ID: process.env["SES_ACCESS_KEY_ID"],
    SECRET_ACCESS_KEY: process.env["SES_SECRET_ACCESS_KEY"],
  },
  SMTP: {
    HOST: process.env["SMTP_HOST"],
    PORT: process.env["SMTP_PORT"],
    USER: process.env["SMTP_USER"],
    PASSWORD: process.env["SMTP_PASSWORD"],
  },
  ELASTIC_SEARCH:{
    HOST:process.env["ELASTIC_SEARCH_HOST"],
  }
};
export default SERVER;
