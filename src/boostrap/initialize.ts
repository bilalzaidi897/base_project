import {  createAdmin } from "../modules/v1/admins/services/admins";
import { adminDao } from "../modules/v1/admins/dao/admins";
import logger from "../services/logger";
const createAdminUser=async()=> {
  try {
    const adminUser = {
      firstName:"demo",
      lastName:"demo",
      email:"demo@yopmail.com",
      password:"Qwerty@123",
      phoneNo:"7088124456",
      countryCode:"+91",
      profilePic:{
        original:"https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        thumbNail:"https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
      },
      addresses:[{
        addressType:"Home",
        country:"India",
        state:"U.P",
        city:"Aligarh",
        address:"Aligarh x",
        latitude:0,
        longitude:0,
        isDefault:true

      }],
      adminType:1,
      status:1
    };
    let isAdminExist=await adminDao.checkAdminExists({email:adminUser.email,phoneNo:adminUser.phoneNo})
    if(!isAdminExist){
        const doc = await createAdmin(adminUser);
        await doc.setPassword(adminUser.password);
        await doc.save();
        console.log('Admin user created successfully.');
    }else {
        console.log('Admin already exists.');
    }
  } catch (error) {
    logger.warn("Error creating admin user.", JSON.stringify(error));
  }
}
export {
    createAdminUser
}